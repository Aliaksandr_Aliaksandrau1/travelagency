<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Travel Agency</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>


	${usr_role}, role in page

	<c:choose>
		<c:when test="${ not empty user and  user.role eq 'customer' }">
			<jsp:forward page="/jsp/customer/customer_page.jsp" />
		</c:when>
		<c:when test="${ not empty user and  user.role eq 'touragent' }">
			<jsp:forward page="/jsp/touragent/touragent_page.jsp" />
		</c:when>
		<c:when test="${ not empty user and  user.role eq 'admin' }">
			<jsp:forward page="/jsp/admin/admin_page.jsp" />
		</c:when>
		<c:otherwise>
		Вставить страницу с ошибкой!!!!!!!!!!!!!
	</c:otherwise>
	</c:choose>

	<h3>Welcome</h3>
	<hr />
	welcome to travel agency!
	<hr />

	<a href="controller?command=logout">Logout</a>

	<a href="jsp/start_page.jsp">Go-Back To start Page</a>

</body>
</html>