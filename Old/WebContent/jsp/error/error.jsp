<!DOCTYPE html>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Error Page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	Request from ${pageContext.errorData.requestURI} is failed
	<br /> Servlet name or type: ${pageContext.errorData.servletName}
	<br /> Status code: ${pageContext.errorData.statusCode}
	<br /> Exception: ${pa geContext.errorData.throwable}
</body>
</html>