<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<title>Header</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>



<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.start_page.head" var="head" />
<fmt:message bundle="${loc}" key="local.start_page.login_button"
	var="login_button" />
<fmt:message bundle="${loc}" key="local.start_page.registration_button"
	var="registration_button" />
<fmt:message bundle="${loc}" key="local.start_page.logout_href"
	var="logout" />


<body>


	<c:import url="${path_page_i18n}" charEncoding="utf-8" />



	<c:choose>
		<c:when test="${empty user  or  user.role eq 'guest' }">
			<a href="Controller?command=registration_go">${registration_button}</a>
			<br>
			<a href="Controller?command=login_go">${login_button}</a>

		</c:when>
		<c:when
			test="${ user.role eq 'customer' or user.role eq 'touragent' or user.role eq 'admin' }">
			<a href="Controller?command=logout"> ${logout} </a>
		</c:when>

	</c:choose>

	<hr />





</body>
</html>

