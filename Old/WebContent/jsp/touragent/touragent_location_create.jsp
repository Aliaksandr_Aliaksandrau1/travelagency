<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Touragent page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>

	<h1>Touragent: Add new Location</h1>
	<form name="CreateLocationForm" method="POST" action="Controller">
		<fieldset>
			<legend> Create Location </legend>
			<input type="hidden" name="command" value="location_create" autocomplete="on" /> <input
				type="text" name="country" value="<c:if test="${not empty entity}">${entity.country}</c:if>" autofocus/> Enter country*: <br /> <input
				type="text" name="city" value="<c:if test="${not empty entity}">${entity.city}</c:if>" /> Enter city*: <br /> <input
				type="text" name="hotel" value="<c:if test="${not empty entity}">${entity.hotel}</c:if>" /> Enter hotel name *: <br /> <input
				type="text" name="description" value="<c:if test="${not empty entity}">${entity.description}</c:if>" /> Enter description *:<br />
		
			 ${locationCreateSuccessMessage} ${locationCreateErrorMessage} <br />
		 <input type="submit"	value="Create location" />
		</fieldset>
	</form>
	<br />
	
	<a href="Controller?command=location_show">Show location list</a>

</body>
</html>