<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.registration_page.head" var="head" />

<html>
<head>
<title>Registration</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
<c:import url="${path_page_header}" charEncoding="utf-8" />
<h1>${head}</h1>
	<form name="RegistrationForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="registration" /> 
		<input type="text" name="usr_login" value="" /> Enter Login *: <br /> 
		<input type="password" name="usr_password" value="" /> Enter Password *: <br />
		<input type="text" name="usr_name" value="" /> Enter your name *: <br />
		<input type="text" name="usr_surname" value="" /> Enter your surname *: <br />
		<input type="text" name="usr_telnumber" value="" /> Enter telephone number *:<br /> 
		<input type="email" name="usr_email" value="" /> Enter email number *:<br /> 
		
				
		
		<c:choose>
			<c:when test="${not empty usr_role and  usr_role eq 'admin' }">
				Enter role *: <select name = "usr_role" size = "1"> 
						<option>customer
						<option>touragent
					</select>
				<br /> 
			</c:when>
			<c:otherwise>
				<input type="hidden" name="usr_role" value="customer" /> <br /> 
			</c:otherwise>
		</c:choose>
		
		
		<c:choose>
			<c:when test="${not empty usr_role and  usr_role eq 'touragent' }">
				<input type="text" name="usr_discount" value="" /> Enter discount *: <br />
				
			</c:when>
			<c:otherwise>
				<input type="hidden" name="usr_discount" value="0" /> <br /> 
			</c:otherwise>
		</c:choose>
		
		
		${errorLoginPassMessage} <br /> ${wrongAction} <br /> ${nullPage} <br />
		<input type="submit" value="Registration" />
	</form>
	<hr />
</body>
</html>
