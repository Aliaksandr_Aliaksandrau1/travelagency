<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">

</head>
<body>

	<h1>Admin Show TourType Page</h1>

	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>


		</tr>
		<c:forEach var="variable" items="${tourTypeList}">
			<tr>
				<td>${variable.id}</td>
				<td>${variable.name}</td>

				<td>
					<table>
						<tr>
							<th>
								<form name="tourTypeDeleteForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="tourtype_delete" />
									<input type="hidden" name="id" value="${variable.id}">
									<input type="submit" value="Delete TourType" />
								</form>
							</th>
							<th>

								<form name="tourTypeUpdateForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="tourtype_update_go" />
									<input type="hidden" name="id" value="${variable.id}">
									<input type="hidden" name="name" value="${variable.name}">
								
									<input type="submit" value="Update TourType" />
								</form>

							</th>
						</tr>
					</table>

			</td>

			</tr>
		</c:forEach>


	</table>
	<br /> ${tourTypeDeleteErrorMessage} ${tourTypeDeleteSuccessMessage} ${tourTypeShowErrorMessage}

	<br />

	<table>
		<tr>
			<th><a href="Controller?command=tourtype_create_go">Add tour
					type</a></th>
			<th><a href="Controller?command=tourtype_show">Refresh List</a>
			</th>
		</tr>
	</table>

</body>
</html>