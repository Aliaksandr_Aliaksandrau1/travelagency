<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Admin page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>

	<h1>Admin: Create new tour status</h1>
	<form method="POST" action="Controller">
		<fieldset>
			<legend> Create tour status </legend>
			<input type="hidden" name="command" value="tourstatus_create" autocomplete="on" /> 
			<input type="text" name="name" value="<c:if test="${not empty entity}">${entity.name}</c:if>" autofocus/> Enter tour status*: <br /> 
				
			${tourStatusCreateSuccessMessage} ${tourStatusCreateErrorMessage} <br />
			 <input type="submit"
				value="Create tour status" />
		</fieldset>
	</form>
	
	<a href="Controller?command=tourstatus_show">Show tour status list</a>

</body>
</html>