<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	
	<h1> Update tour</h1>
	<form method="POST" action="Controller">
		<input type="hidden" name="command" value="tour_update" />
			<input type="hidden" name="id" value="${entity.id}" /> 
			<input type="text" name="title" value="${entity.title}" /> Enter name*: <br />
			<input type="date" name="begin_date" value="${entity.beginDate}" min="2015-01-01" /> Enter begin date of tour*: <br />
			<input type="date" name="end_date" value="${entity.endDate}" min="2015-01-01" /> Enter end date of tour*: <br />
			<input type="text" name="price" value="${entity.price}"/> Enter tour price*: <br />
			<input type="text" name="description" value="${entity.description}"/> Enter description*: <br />
			
			<select name="location">
				<c:forEach var="variable" items="${locationList}">
						<option value="${variable.id}"> ${variable.country}, ${variable.city}, ${variable.hotel}</option>
				</c:forEach> 
			</select>	Choose location:

				<br/>
			
			<c:forEach var="variable" items="${locationList}">
					
				<input type="checkbox" name="location1" value="${variable.id}"> ${variable.country}, ${variable.city}, ${variable.hotel}
				<br/>
			
			</c:forEach> 
			
			
			<select name="tourtype">
				<c:forEach var="variable" items="${tourTypeList}">
						<option value="${variable.id}"> ${variable.name} </option>
				</c:forEach> 
			</select>	Choose tour type:
			<br/>
			<select name="tourstatus">
				<c:forEach var="variable" items="${tourStatusList}">
						<option value="${variable.id}"> ${variable.name} </option>
				</c:forEach> 
			</select>	Choose tour status:
<br/>

	
			<input type="submit" value="Update tour" />
	</form>



</body>
</html>