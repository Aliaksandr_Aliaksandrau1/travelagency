<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tour</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">

</head>
<body>

	<h1>Tour Catalogue</h1>

	<table>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Begin Date</th>
			<th>End Date</th>
			<th>Tour Type</th>
			<th>Tour Status</th>
			<th>Description</th>
			<th>Price, $</th>
			<th>Country</th>
			<th>City</th>

		</tr>
		<c:forEach var="variable" items="${tourList}">
		
			<tr>
				<td>${variable.id}</td>
				<td>${variable.title}</td>
				<td>${variable.beginDate}</td>
				<td>${variable.endDate}</td>
				<td>${variable.tourType.name}</td>
				<td>${variable.tourStatus.name}</td>
				<td>${variable.description}</td>
				<td>${variable.price}</td>
				<td> <c:forEach var="var" items="${variable.locList}">	
					${var.country}, ${var.city}, ${var.hotel}
					</c:forEach>		
						
				</td>
				<td> 
					<c:forEach var="var" items="${variable.locList}">	
					${var.city}
					</c:forEach>		
				</td>
				<c:choose>
				<c:when test="${ not empty usr_role  and  usr_role eq 'touragent' }">
				<td>
					<table>
						<tr>
							<th>
								<form method="POST" action="Controller">
									<input type="hidden" name="command" value="tour_delete" />
									<input type="hidden" name="id" value="${variable.id}">
									<input type="submit" value="Delete Tour" />
								</form>
							</th>
							<th>

								<form method="POST" action="Controller">
									<input type="hidden" name="command"
										value="tour_update_go" /> <input type="hidden"
										name="id" value="${variable.id}">

									<input type="submit" value="Update Tour" />
								</form>

							</th>
						</tr>
					</table>
				
				</td>
				</c:when>
				<c:when test="${ not empty usr_role and  usr_role eq 'guest' }">
				<td>
				<form method="POST" action="Controller">
					<input type="hidden" name="command" value="order_create" />
					<input type="hidden" name="tourId" value="${variable.id}">
					<input type="hidden" name="orderStatusId" value="5">
					<input type="hidden" name="userId" value="2"> 
					<input type="submit" value="Make order" />
				</form>
				
				</td>
				
				</c:when>
         </c:choose>


			</tr>
		</c:forEach>
	</table>



	<br />

	<table>
		<tr>
			
			
			<th><a href="Controller?command=tour_show">Refresh
					List</a></th>
		</tr>
	</table>


			<a href="Controller?command=tour_add_go">Add tour	</a>
		
				
</body>
</html>