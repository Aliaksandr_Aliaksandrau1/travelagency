<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Admin page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>

	<h1>Admin: Create new tour</h1>
	<form method="POST" action="Controller">
		
		
			<input type="hidden" name="command" value="tour_add" autocomplete="on" /> 
			
			<input type="text" name="title" value="" autofocus/> Enter tour title*: <br />
			
			<input type="date" name="begin_date" min="2015-01-01" /> Enter begin date of tour*: <br />
			<input type="date" name="end_date" min="2015-01-01" /> Enter end date of tour*: <br />
			
			
			<input type="text" name="price" value=""/> Enter tour price*: <br />
			<input type="text" name="description" value=""/> Enter tour description*: <br />
				
			
			<select name="location">
				<c:forEach var="variable" items="${locationList}">
						<option value="${variable.id}"> ${variable.country}, ${variable.city}, ${variable.hotel}</option>
				</c:forEach> 
			</select>	Choose location:
			
			<br/>
			
			<select name="tourtype">
				<c:forEach var="variable" items="${tourTypeList}">
						<option value="${variable.id}"> ${variable.name} </option>
				</c:forEach> 
			</select>	Choose tour type:
			
			<br/>
			
			<select name="tourstatus">
				<c:forEach var="variable" items="${tourStatusList}">
						<option value="${variable.id}"> ${variable.name} </option>
				</c:forEach> 
			</select>	Choose tour status:
			
			<br/>
				
				
			${locationaddsuccess} <br /> ${errorLocationAddMessage} <br />
			${wrongAction} <br /> ${nullPage} <br /> <input type="submit"
				value="Create tour" />
		
	</form>
	
	<a href="Controller?command=tour_show">Show tour list</a>

</body>
</html>