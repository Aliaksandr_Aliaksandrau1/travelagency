package resource;

import java.util.ResourceBundle;

public class ValidationRegExManager {
	private final static ResourceBundle resourceBundle = ResourceBundle
			.getBundle("resources.validationregex");
	
	private ValidationRegExManager() {
	}

	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
