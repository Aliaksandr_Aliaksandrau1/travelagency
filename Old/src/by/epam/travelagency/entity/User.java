package by.epam.travelagency.entity;

// �������� ��������� HttpSessionBindingListener (���������� user � ������)


public class User extends Entity {
	private static final long serialVersionUID = 1L;
	private String login;
	private String password;
	private String role;
	private String name;
	private String surname;
	private String telephoneNumber;
	private String email;
	private double discount;

	public User() {
	}
	
	public User(int id, String login, String password, String role, String name,
			String surname, String telephoneNumber, String email,
			double discount) {
		super (id);
		this.login = login;
		this.password = password;
		this.role = role;
		this.name = name;
		this.surname = surname;
		this.telephoneNumber = telephoneNumber;
		this.email = email;
		this.discount = discount;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String status) {
		this.role = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		return "User [login=" + login + ", password=" + password + ", role="
				+ role + ", name=" + name + ", surname=" + surname
				+ ", telephoneNumber=" + telephoneNumber + ", email=" + email
				+ ", discount=" + discount + ", getId()=" + getId() + "]";
	}

}
