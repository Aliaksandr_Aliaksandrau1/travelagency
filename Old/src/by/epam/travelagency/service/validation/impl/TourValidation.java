package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import resource.ValidationRegExManager;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.validation.Validation;


public class TourValidation extends Validation<Tour> {
		
	
	public boolean isEntityValid(Tour entity) {
		boolean isChecked = false;

		if (checkData(entity.getTitle(),ValidationRegExManager
				.getProperty("validation.regex.tour.title"))) {
			isChecked = true;
		}

		return isChecked;
	}
	
	
	
	public boolean isInputParameterMapValid (Map <String, String> entityParameterMap) {
		boolean isChecked = false;
		String title = entityParameterMap.get("title");
		//String price = entityParameterMap.get("price");
		System.out.println(title);
		
		
		if (checkData(title ,ValidationRegExManager
				.getProperty("validation.regex.tour.title"))) {
			isChecked = true;
		}

		
			
		return isChecked;
	}
	
	
	
	
	
}


// �������� ��������� ��� ��������� �����  !!!!!!!!!!!!!!!!

