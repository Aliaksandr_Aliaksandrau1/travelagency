package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.validation.Validation;

public class UserValidation extends Validation<User> {
	String regexRegistrationLogin = "[\\w]{5,20}"; 
	String regexRegistrationPassword = "[\\w]{5,20}";
	String regexRegistrationRole = "(customer)|(touragent)|(admin)";
	String regexRegistrationName = "[a-zA-Z�-��-�-]{1,100}";
	String regexRegistrationSurname = "[a-zA-Z�-��-�-]{1,100}";
	String regexRegistrationTelnumber = "[\\d]{7,11}";
	String regexRegistrationEmail = "([.[^@\\s]]+)@([.[^@\\s]]+)\\.([a-z]+)";
	String regexRegistrationDiscount = "([0-9]{0,3}.[0-9]{0,4})"; // ����������
																	// ���������

	
	public boolean isEntityValid(User entity) {
		boolean isChecked = false;

		if (checkData(entity.getLogin(), regexRegistrationLogin)
				&& checkData(entity.getPassword(), regexRegistrationPassword)
				&& checkData(entity.getRole(), regexRegistrationRole)
				&& checkData(entity.getName(), regexRegistrationName)
				&& checkData(entity.getSurname(), regexRegistrationSurname)
				&& checkData(entity.getTelephoneNumber(),
						regexRegistrationTelnumber)
				&& checkData(entity.getEmail(), regexRegistrationEmail)
				&& checkData(entity.getDiscount(), regexRegistrationDiscount) //
		) {
			isChecked = true;
		}

		return isChecked;
	}

	
	
	
	
	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		// TODO Auto-generated method stub
		return false;
	}

}
