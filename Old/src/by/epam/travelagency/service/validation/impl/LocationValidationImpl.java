package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import resource.ValidationRegExManager;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.validation.Validation;

public class LocationValidationImpl extends Validation<Location> {
	private LocationValidationImpl() {
	}

	public final static LocationValidationImpl instance = new LocationValidationImpl();

	public static LocationValidationImpl getInstance() {
		return instance;
	}

	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		boolean isChecked = false;
		String country = entityParameterMap.get("country");
		String city = entityParameterMap.get("city");
		String hotel = entityParameterMap.get("hotel");
		String description = entityParameterMap.get("description");

		String regexCountry = ValidationRegExManager
				.getProperty("validation.regex.location.country");
		String regexCity = ValidationRegExManager
				.getProperty("validation.regex.location.city");
		String regexHotel = ValidationRegExManager
				.getProperty("validation.regex.location.hotel");
		String regexDescription = ValidationRegExManager
				.getProperty("validation.regex.location.description");

		if (checkData(country, regexCountry) && checkData(city, regexCity)
				&& checkData(hotel, regexHotel)
				&& checkData(description, regexDescription)) {

			isChecked = true;
		}

		return isChecked;
	}

}
