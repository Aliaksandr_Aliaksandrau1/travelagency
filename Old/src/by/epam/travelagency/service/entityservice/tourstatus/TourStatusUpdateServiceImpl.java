package by.epam.travelagency.service.entityservice.tourstatus;

import java.util.Map;

import by.epam.travelagency.dao.impl.TourStatusDAOImpl;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.entityservice.IEntityUpdateService;
import by.epam.travelagency.service.validation.impl.TourStatusValidationImpl;

public class TourStatusUpdateServiceImpl implements
IEntityUpdateService<TourStatus>{

	private TourStatusUpdateServiceImpl() {
	}
	
	private final static TourStatusUpdateServiceImpl instance = new TourStatusUpdateServiceImpl();

	public static TourStatusUpdateServiceImpl getInstance() {
		return instance;
	}


	@Override
	public TourStatus entityUpdate(Map<String, String> entityParameterMap) {
		TourStatus entity = null;

		if (TourStatusValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new TourStatus();
			int id = Integer.parseInt(entityParameterMap.get("id"));
			String name = entityParameterMap.get("name");
			entity.setId(id);
			entity.setName(name);

			if (!TourStatusDAOImpl.getInstance().update(entity)) {
				entity = null;
			}

		}
		return entity;
	}
		
	
}
