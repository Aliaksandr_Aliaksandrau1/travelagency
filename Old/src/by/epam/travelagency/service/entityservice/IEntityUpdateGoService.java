package by.epam.travelagency.service.entityservice;

import by.epam.travelagency.entity.Entity;

public interface IEntityUpdateGoService<T extends Entity> {

	public T entityToUpdate (int id);

}
