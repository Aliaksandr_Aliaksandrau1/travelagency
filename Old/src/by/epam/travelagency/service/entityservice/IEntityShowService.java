package by.epam.travelagency.service.entityservice;

import java.util.List;

import by.epam.travelagency.entity.Entity;

public interface IEntityShowService <T extends Entity> {
	public  List <T> entityShow();
}
