package by.epam.travelagency.service.entityservice.user;



import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.validation.impl.UserValidation;

public class RegistrationService {

	public RegistrationService() {
	}

	UserValidation validator = new UserValidation();

	public boolean userRegistration(String login, String pass, String role,
			String name, String surname, String telnumber, String email,
			double discount) {
		boolean successRegistration = false;

		User user = new User(1, login, pass, role, name, surname, telnumber,
				email, discount);

		if (validator.isEntityValid(user)) {

			UserDAOImpl.getInstance().create(user);

			successRegistration = true;
			// �������� ����� ���������� �� ���� ������ User � ��������� ���
			// � ���������� ��������� ???????
		}

		return successRegistration;
	}

	/*
	 * private boolean loginOccupied(String enterLogin) { boolean isOccupied =
	 * false;
	 * 
	 * if
	 * (enterLogin.equals(UserDAOImpl.getInstance().isLoginOccupied(enterLogin
	 * ))) { isOccupied = true; }
	 * 
	 * // System.out.println("registrationLogic  - loginOccupied " + //
	 * isOccupied); return isOccupied; }
	 */

}
