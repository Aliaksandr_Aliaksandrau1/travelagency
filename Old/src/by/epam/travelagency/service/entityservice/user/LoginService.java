package by.epam.travelagency.service.entityservice.user;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;

public class LoginService {

	

	public static User checkLogin(String enterLogin, String enterPass) {

		User user = null;
		User userDB = UserDAOImpl.getInstance().checkLogin(enterLogin,
				enterPass);

		if (userDB != null) {
			if (enterLogin.equals(userDB.getLogin())
					&& enterPass.equals(userDB.getPassword())) {
				user = userDB;
			}
		}
		return user;
		
	}

}
