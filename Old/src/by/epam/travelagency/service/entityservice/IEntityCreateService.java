package by.epam.travelagency.service.entityservice;

import java.util.Map;

import by.epam.travelagency.entity.Entity;

public interface IEntityCreateService <T extends Entity> {

	//public boolean entityCreate(Map<String, String> entityParameterMap);
	public T entityCreateNew(Map<String, String> entityParameterMap);

}
