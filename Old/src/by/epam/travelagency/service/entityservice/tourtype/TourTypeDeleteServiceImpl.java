package by.epam.travelagency.service.entityservice.tourtype;

import by.epam.travelagency.dao.impl.TourTypeDAOImpl;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class TourTypeDeleteServiceImpl implements IEntityDeleteService {

	private TourTypeDeleteServiceImpl() {
	}

	private final static TourTypeDeleteServiceImpl instance = new TourTypeDeleteServiceImpl();

	public static TourTypeDeleteServiceImpl getInstance() {
		return instance;
	}

	@Override
	public boolean entityDeleteNew(int id) {
		return TourTypeDAOImpl.getInstance().delete(id);
	}

}
