package by.epam.travelagency.service.entityservice.order;



import java.util.List;

import by.epam.travelagency.dao.impl.OrderDAOImpl;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.service.entityservice.IEntityShowService;



public class OrderShowService implements  IEntityShowService <Order> {
	private OrderShowService() {
	}

	private final static OrderShowService instance = new OrderShowService();

	public static OrderShowService getInstance() {
		return instance;
	}
	
	@Override
	
	public List<Order> entityShow() {
		List<Order> orderList;
	
		orderList = OrderDAOImpl.getInstance().findAll();
		
		
		// ��������� ���� ��� ������� �� ������� 
		
		for	(Order order : orderList) {
			double price = order.getTour().getPrice();
			double discount = order.getUser().getDiscount();
			double tourPriceForUser = price - (price*discount/100);
			order.getTour().setPrice(tourPriceForUser);
		}

		return orderList;
	}
}