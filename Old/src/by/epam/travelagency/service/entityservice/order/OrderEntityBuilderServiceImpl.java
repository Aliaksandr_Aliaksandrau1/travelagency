package by.epam.travelagency.service.entityservice.order;

import java.util.Date;
import java.util.Map;


import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.Tour;

import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.EntityBuilderService;
import by.epam.travelagency.service.validation.impl.OrderValidationImpl;


public class OrderEntityBuilderServiceImpl implements EntityBuilderService<Order> {

	
	private OrderEntityBuilderServiceImpl() {
	}

	private final static OrderEntityBuilderServiceImpl instance = new OrderEntityBuilderServiceImpl();

	public static OrderEntityBuilderServiceImpl getInstance() {
		return instance;
	}
		
	@Override
	public Order buildEntity (Map <String, String> entityParameterMap) {
		Order entity = null;
		//OrderValidationImpl validator = new OrderValidationImpl();

		
		
		
		if (OrderValidationImpl.getInstance().isInputParameterMapValid(entityParameterMap)) {   // ���� ��������� ��������, ��������� ������
			
			
			
			entity = new Order();
			
			if (entityParameterMap.containsKey("id")) {
				
				int orderId = Integer.parseInt(entityParameterMap.get("id"));
				entity.setId(orderId);
			}
			
			int tourId = Integer.parseInt(entityParameterMap
					.get("tourId"));	
			int userId = Integer.parseInt(entityParameterMap
					.get("userId"));
			int orderStatusId = Integer.parseInt(entityParameterMap
					.get("orderStatusId"));			
			
			
			Tour tour = new Tour();            // �������� �����	 �� ��
			User user = new User();				// �������� �����  �� ��
			OrderStatus orderStatus = new OrderStatus();	// �������� �����  �� ��
			
						
			entity.setTour(tour);
			entity.setUser(user);
			entity.setOrderStatus(orderStatus);

			
			System.out.println(entity); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
		}
				
		return entity; 
		
	}

}
