package by.epam.travelagency.service.entityservice;

import java.util.Map;

import by.epam.travelagency.entity.Entity;

public interface IEntityUpdateService<T extends Entity> {

	public T entityUpdate (Map <String, String> entityParameterMap);

}
