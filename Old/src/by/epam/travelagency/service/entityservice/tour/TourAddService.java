package by.epam.travelagency.service.entityservice.tour;


import java.util.Map;


import by.epam.travelagency.dao.impl.TourDAOImpl;
import by.epam.travelagency.entity.Tour;


public class TourAddService {

	public TourAddService() {
	}

	public boolean entityAdd (Map <String, String> entityParameterMap) {

		boolean successEntityAdd = false;
		Tour entity = TourEntityBuilderServiceImpl.getInstance().buildEntity(entityParameterMap);
		if (entity != null)	 {
			
			TourDAOImpl.getInstance().create(entity);
			successEntityAdd = true;
		}
		return successEntityAdd;
	}
}
