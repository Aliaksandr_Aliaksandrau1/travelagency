package by.epam.travelagency.service.entityservice.tour;

import java.util.Date;
import java.util.Map;



import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.EntityBuilderService;
import by.epam.travelagency.service.validation.DateFormatValidationService;
import by.epam.travelagency.service.validation.impl.TourValidation;

public class TourEntityBuilderServiceImpl implements EntityBuilderService<Tour> {

	
	private TourEntityBuilderServiceImpl() {
	}

	private final static TourEntityBuilderServiceImpl instance = new TourEntityBuilderServiceImpl();

	public static TourEntityBuilderServiceImpl getInstance() {
		return instance;
	}
		
	@Override
	public Tour buildEntity (Map<String, String> entityParameterMap) {
		Tour entity = null;
		TourValidation validator = new TourValidation();

		
		
		
		if (validator.isInputParameterMapValid(entityParameterMap)) {   // ���� ��������� ��������, ��������� ������
			
			
			
			entity = new Tour();
			
			if (entityParameterMap.containsKey("id")) {
				
				int tourId = Integer.parseInt(entityParameterMap.get("id"));
				entity.setId(tourId);
			}
				
			
			TourStatus tourStatusEntity = new TourStatus();
			TourType tourTypeEntity = new TourType();
		
			String title = entityParameterMap.get("title");

			int tourTypeId = Integer.parseInt(entityParameterMap
					.get("tourTypeId"));
			int tourStatusId = Integer.parseInt(entityParameterMap
					.get("tourStatusId"));
			double price = Double.parseDouble(entityParameterMap.get("price"));
			String description = entityParameterMap.get("description");
			int locationId = Integer.parseInt(entityParameterMap
					.get("location"));
			
			
			
			Date beginDate = DateFormatValidationService.getInstance()
					.dateFormatterFromStringToDate(
							entityParameterMap.get("beginDate"));
			
		
			
			Date endDate = DateFormatValidationService.getInstance()
					.dateFormatterFromStringToDate(
							entityParameterMap.get("endDate"));
		
			
			
			
			tourTypeEntity.setId(tourTypeId);
			tourStatusEntity.setId(tourStatusId);

			entity.setTitle(title);
			entity.setBeginDate(beginDate);
			entity.setEndDate(endDate);
			entity.setTourType(tourTypeEntity);
			entity.setTourStatus(tourStatusEntity);
			entity.setPrice(price);
			entity.setDescription(description);

			
			System.out.println(entity); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
		}
		return entity;
	}

}
