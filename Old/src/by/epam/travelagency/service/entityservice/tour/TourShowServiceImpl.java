package by.epam.travelagency.service.entityservice.tour;


import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.epam.travelagency.dao.impl.Location2TourDAOImpl;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.dao.impl.TourDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.Location2Tour;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.entityservice.IEntityShowService;



public class TourShowServiceImpl implements  IEntityShowService <Tour> {
	private TourShowServiceImpl() {
	}

	private final static TourShowServiceImpl instance = new TourShowServiceImpl();

	public static TourShowServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	
	public List<Tour> entityShow() {
		List<Tour> tourList;
		List<Location> locationList;
		List<Location2Tour> location2TourList;

		tourList = TourDAOImpl.getInstance().findAll();
		locationList = LocationDAOImpl.getInstance().findAll();
		location2TourList = Location2TourDAOImpl.getInstance().findAll();

		Map<Integer, Tour> tourMap = new HashMap<Integer, Tour>();
		Map<Integer, Location> locationMap = new HashMap<Integer, Location>();

		for (Tour tour : tourList) {											// �������� �� �����������!!!
			tourMap.put(tour.getId(), tour);
		}
		for (Location location : locationList) {
			locationMap.put(location.getId(), location);
		}

		for (Location2Tour l2t : location2TourList) {

			tourMap.get(l2t.getTourId()).getLocationsInTourMap()
					.put(l2t.getId(), locationMap.get(l2t.getLocationId()));   // ���������� ������� � map ������� ����
		
			tourMap.get(l2t.getTourId()).getLocList().add(locationMap.get(l2t.getLocationId()));
		
		
		}

		Collection<Tour> tourCollection = tourMap.values();
		
		tourList.clear();
		for (Tour tour : tourCollection) {
			tourList.add(tour);
		}
	
		
		return tourList;
	}
}