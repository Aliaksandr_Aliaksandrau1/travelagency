package by.epam.travelagency.service.entityservice.orderstatus;

import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;

import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class OrderStatusDeleteServiceImpl implements
		IEntityDeleteService  {

	private OrderStatusDeleteServiceImpl() {
	}

	private final static OrderStatusDeleteServiceImpl instance = new OrderStatusDeleteServiceImpl();

	public static OrderStatusDeleteServiceImpl getInstance() {
		return instance;
	}

	
	
	
	@Override
	public boolean entityDeleteNew (int id) {
	
		return OrderStatusDAOImpl.getInstance().delete(id);
	}
	
	

}
