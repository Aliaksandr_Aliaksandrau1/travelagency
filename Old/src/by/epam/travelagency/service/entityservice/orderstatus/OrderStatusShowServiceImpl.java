package by.epam.travelagency.service.entityservice.orderstatus;

import java.util.List;

import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.IEntityShowService;

public class OrderStatusShowServiceImpl implements IEntityShowService<OrderStatus> {
	private OrderStatusShowServiceImpl() {
	}
	private final static OrderStatusShowServiceImpl instance = new OrderStatusShowServiceImpl();
	public static OrderStatusShowServiceImpl getInstance() {
		return instance;
	}

	@Override
	public List<OrderStatus> entityShow() {
		List<OrderStatus> entityList = null;
		entityList = OrderStatusDAOImpl.getInstance().findAll();
		return entityList;
	}

}
