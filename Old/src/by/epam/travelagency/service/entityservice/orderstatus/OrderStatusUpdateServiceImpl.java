package by.epam.travelagency.service.entityservice.orderstatus;

import java.util.Map;
import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.IEntityUpdateService;
import by.epam.travelagency.service.validation.impl.OrderStatusValidationImpl;

public class OrderStatusUpdateServiceImpl implements
		IEntityUpdateService<OrderStatus> {

	private OrderStatusUpdateServiceImpl() {
	}

	private final static OrderStatusUpdateServiceImpl instance = new OrderStatusUpdateServiceImpl();

	public static OrderStatusUpdateServiceImpl getInstance() {
		return instance;
	}

	@Override
	public OrderStatus entityUpdate(Map<String, String> entityParameterMap) {
		OrderStatus entity = null;

		if (OrderStatusValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new OrderStatus();
			int id = Integer.parseInt(entityParameterMap.get("id"));
			String name = entityParameterMap.get("name");
			entity.setId(id);
			entity.setName(name);

			if (!OrderStatusDAOImpl.getInstance().update(entity)) {
				entity = null;
			}

		}
		return entity;
	}

}
