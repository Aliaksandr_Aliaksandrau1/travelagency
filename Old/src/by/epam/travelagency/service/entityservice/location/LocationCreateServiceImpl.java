package by.epam.travelagency.service.entityservice.location;

import java.util.Map;

import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.validation.impl.LocationValidationImpl;


public class LocationCreateServiceImpl implements
		IEntityCreateService<Location> {

	private LocationCreateServiceImpl() {
	}

	private final static LocationCreateServiceImpl instance = new LocationCreateServiceImpl();

	public static LocationCreateServiceImpl getInstance() {
		return instance;
	}

	@Override
	public Location entityCreateNew(Map<String, String> entityParameterMap) {
		Location entity = null;
		if (LocationValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new Location();

			String country = entityParameterMap.get("country");
			String city = entityParameterMap.get("city");
			String hotel = entityParameterMap.get("hotel");
			String description = entityParameterMap.get("description");

			entity.setCountry(country);
			entity.setCity(city);
			entity.setHotel(hotel);
			entity.setDescription(description);

			if (!LocationDAOImpl.getInstance().create(entity)) {
				entity = null;
			}
		}

		return entity;
	}

}
