package by.epam.travelagency.service.entityservice.location;

import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class LocationDeleteServiceImpl implements IEntityDeleteService  {

	private LocationDeleteServiceImpl() {
	}
	private final static LocationDeleteServiceImpl instance = new LocationDeleteServiceImpl();

	public static LocationDeleteServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public boolean entityDeleteNew(int id) {
		
		return LocationDAOImpl.getInstance().delete(id);
	}
	
}
