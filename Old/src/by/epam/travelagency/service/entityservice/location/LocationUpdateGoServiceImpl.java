package by.epam.travelagency.service.entityservice.location;

import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.entityservice.IEntityUpdateGoService;

public class LocationUpdateGoServiceImpl implements
		IEntityUpdateGoService<Location> {

	private LocationUpdateGoServiceImpl() {
	}

	private final static LocationUpdateGoServiceImpl instance = new LocationUpdateGoServiceImpl();

	public static LocationUpdateGoServiceImpl getInstance() {
		return instance;
	}

	@Override
	public Location entityToUpdate(int id) {
		Location entity = null;
		entity = LocationDAOImpl.getInstance().findEntityById(id);
		return entity;
	}

}
