package by.epam.travelagency.service.entityservice.location;


import java.util.Map;

import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.entityservice.IEntityUpdateService;
import by.epam.travelagency.service.validation.impl.LocationValidationImpl;


public  class LocationUpdateServiceImpl implements
IEntityUpdateService<Location> {

	private LocationUpdateServiceImpl() {
	}
	private final static LocationUpdateServiceImpl instance = new LocationUpdateServiceImpl();

	public static LocationUpdateServiceImpl getInstance() {
		return instance;
	}
	

	@Override
	public Location entityUpdate(Map<String, String> entityParameterMap) {
		Location entity = null;

		if (LocationValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new Location();
			
			int id = Integer.parseInt(entityParameterMap.get("id"));
			String country = entityParameterMap.get("country");
			String city = entityParameterMap.get("city");
			String hotel = entityParameterMap.get("hotel");
			String description = entityParameterMap.get("description");
			
			entity.setId(id);
			entity.setCountry(country);
			entity.setCity(city);
			entity.setHotel(hotel);
			entity.setDescription(description);
			
					
			if (!LocationDAOImpl.getInstance().update(entity)) {
				entity = null;
			}

		}
		return entity;
	}
}
