package by.epam.travelagency.service.entityservice.location;

import java.util.List;

import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.entityservice.IEntityShowService;

public class LocationShowServiceImpl implements IEntityShowService<Location> {
	private LocationShowServiceImpl() {
	}

	private final static LocationShowServiceImpl instance = new LocationShowServiceImpl();

	public static LocationShowServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public List<Location> entityShow() {
		List<Location> entityList = LocationDAOImpl.getInstance().findAll();
		return entityList;
	}

}
