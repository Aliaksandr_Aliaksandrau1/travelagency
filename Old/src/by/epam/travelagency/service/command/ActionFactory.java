package by.epam.travelagency.service.command;

import javax.servlet.http.HttpServletRequest;


import by.epam.travelagency.service.command.impl.EmptyCommand;
import resource.ConfigurationManager;
import resource.MessageManager;

public class ActionFactory {
	private ActionFactory() { 
	}

	private final static ActionFactory instance = new ActionFactory();

	public static ActionFactory getInstance() {
		return instance;
	}
		
	public ActionCommand defineCommand(HttpServletRequest request) {
		
		
		
		ActionCommand current = new EmptyCommand();  // �������� ����� ����� enum
		
		String action = request.getParameter("command");
		
		
		
	//	request.setAttribute("path_page_i18n", ConfigurationManager.getProperty("path.page.i18n"));
	//	request.setAttribute("path_page_header", ConfigurationManager.getProperty("path.page.header"));
	//	request.setAttribute("path_page_footer", ConfigurationManager.getProperty("path.page.footer"));
		request.setAttribute("path_page_start_page", ConfigurationManager.getProperty("path.page.start_page"));
	//	request.setAttribute("path_page_tour_catalog", ConfigurationManager.getProperty("path.page.tour_catalog"));
	//	request.setAttribute("path_style_css", ConfigurationManager.getProperty("path.page.style_css"));
	
		
		System.out.println("Command name = " + action); // ������
		
		if (action == null || action.isEmpty()) {
						
			return current;
		}
		
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute("wrongAction",
					action + MessageManager.getProperty("message.wrongaction"));
		}
		return current;
	}
}
