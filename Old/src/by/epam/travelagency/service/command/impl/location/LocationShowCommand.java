package by.epam.travelagency.service.command.impl.location;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;

public class LocationShowCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<Location> entityList = new ArrayList<Location>();
		String page = null;

		entityList = LocationShowServiceImpl.getInstance().entityShow();
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.show");

		if (entityList != null) {

			request.setAttribute("locationList", entityList);

		} else {
			request.setAttribute("locationShowErrorMessage",
					MessageManager.getProperty("message.location.show.error"));

		}
		return page;
	}
}
