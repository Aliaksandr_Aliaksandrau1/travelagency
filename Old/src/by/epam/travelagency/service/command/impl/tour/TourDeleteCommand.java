package by.epam.travelagency.service.command.impl.tour;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;





import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.command.ActionCommand;

import by.epam.travelagency.service.entityservice.tour.TourDeleteServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class TourDeleteCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int id = Integer.parseInt(request
				.getParameter(NamedConstant.PARAM_TOUR_ID));

		

		List<Tour> entityList = null;
		
		entityList = TourDeleteServiceImpl.getInstance().entityDelete(id);
		
		if (entityList != null) {

			request.setAttribute("tourList", entityList);
			page = ConfigurationManager
					.getProperty("path.page.tour.show");

		}

		else {
			request.setAttribute("errorOrderStatusDeleteMessage",
					MessageManager.getProperty("message.orderstatusdeleteerror"));
			page = ConfigurationManager
					.getProperty("path.page.tour.show");   // доработать
		}

		return page;
	}

}
