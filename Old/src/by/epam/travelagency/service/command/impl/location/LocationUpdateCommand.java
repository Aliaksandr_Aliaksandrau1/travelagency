package by.epam.travelagency.service.command.impl.location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.location.LocationDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationUpdateServiceImpl;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusUpdateServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class LocationUpdateCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		Map<String, String> entityParameterMap = new HashMap<String, String>();

		String id = request.getParameter(NamedConstant.PARAM_LOCATION_ID)
				.trim();
		String country = request.getParameter(
				NamedConstant.PARAM_LOCATION_COUNTRY).trim();
		String city = request.getParameter(NamedConstant.PARAM_LOCATION_CITY)
				.trim();
		String hotel = request.getParameter(NamedConstant.PARAM_LOCATION_HOTEL)
				.trim();
		String description = request.getParameter(
				NamedConstant.PARAM_LOCATION_DESCRIPTION).trim();

		entityParameterMap.put("id", id);
		entityParameterMap.put("country", country);
		entityParameterMap.put("city", city);
		entityParameterMap.put("hotel", hotel);
		entityParameterMap.put("description", description);

		Location entity = LocationUpdateServiceImpl.getInstance().entityUpdate(
				entityParameterMap);
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.update");

		if (entity != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("locationUpdateSuccessMessage", MessageManager
					.getProperty("message.location.update.success"));

		} else {

			request.setAttribute("locationUpdateErrorMessage",
					MessageManager.getProperty("message.location.update.error"));

		}
		return page;
	}

}
