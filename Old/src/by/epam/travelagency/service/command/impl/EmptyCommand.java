package by.epam.travelagency.service.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.travelagency.service.command.ActionCommand;
import resource.ConfigurationManager;

public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = ConfigurationManager.getProperty("path.page.start_page");
		return page;
	}
}
