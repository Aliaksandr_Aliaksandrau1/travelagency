package by.epam.travelagency.service.command.impl.tourtype;

import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;


import resource.ConfigurationManager;


public class TourTypeCreateGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
			
		String page = ConfigurationManager.getProperty("path.page.admin.tourtype.create");
			
		return page;
	}

}



