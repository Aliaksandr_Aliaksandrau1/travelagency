package by.epam.travelagency.service.command.impl.location;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;

import by.epam.travelagency.service.command.ActionCommand;

import by.epam.travelagency.service.entityservice.location.LocationDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;

import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class LocationDeleteCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		List<Location> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request.getParameter(
				NamedConstant.PARAM_LOCATION_ID).trim());

		isDeleted = LocationDeleteServiceImpl.getInstance().entityDeleteNew(id);

		entityList = LocationShowServiceImpl.getInstance().entityShow();

		request.setAttribute("locationList", entityList);
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.show");

		if (isDeleted) {

			request.setAttribute("locationDeleteSuccessMessage", MessageManager
					.getProperty("message.location.delete.success"));

		}

		else {

			request.setAttribute("orderStatusDeleteErrorMessage",
					MessageManager.getProperty("message.location.delete.error"));
		}

		return page;
	}

}
