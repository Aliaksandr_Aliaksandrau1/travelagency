package by.epam.travelagency.service.command.impl.tour;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.command.ActionCommand;

import by.epam.travelagency.service.entityservice.tour.TourShowServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;

public class TourShowCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<Tour> tourList = new ArrayList<Tour>();
		
		
	
		
		
		String page = null;

		
			tourList = TourShowServiceImpl.getInstance().entityShow();
		

		
		if (tourList != null) {

			request.setAttribute("tourList", tourList);
			
			page = ConfigurationManager
					.getProperty("path.page.tour.show");

		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.tour.show"); // доработать
		}
		return page;
	}
}
