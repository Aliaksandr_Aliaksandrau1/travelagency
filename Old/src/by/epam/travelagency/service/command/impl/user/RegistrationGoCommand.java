package by.epam.travelagency.service.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.user.RegistrationService;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class RegistrationGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		
		logger.debug("RegistrationGo command"); // ������
		
		
		String page = null;
		page = ConfigurationManager.getProperty("path.page.registration");
		
		
		return page;
	}

}



