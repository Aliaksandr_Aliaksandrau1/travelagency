package by.epam.travelagency.service.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;

public class I18nCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		request.getSession(true).setAttribute("local",
				request.getParameter("local"));

		String pathURI = request.getSession(true).getAttribute("path_uri")
				.toString();
		String context_path = request.getSession(true)
				.getAttribute("context_path").toString();

		page = pathURI.split(context_path)[1];

		return page;
	}
}
