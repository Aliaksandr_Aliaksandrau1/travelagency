package by.epam.travelagency.service.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;
import resource.ConfigurationManager;
import resource.NamedConstant;

public class LogoutCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	@Override
	public String execute(HttpServletRequest request) {
		logger.debug("LogoutCommand");
		
		
		String page = ConfigurationManager.getProperty("path.page.start_page");
		
		
				
		request.getSession().invalidate();
		request.getSession().setAttribute(NamedConstant.PARAM_USER_ROLE,  NamedConstant.PARAM_USER_ROLE_BY_DEFAULT);
		

		
		
		return page;
	}

}
