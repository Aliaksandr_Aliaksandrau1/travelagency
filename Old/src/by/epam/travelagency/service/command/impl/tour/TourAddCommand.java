package by.epam.travelagency.service.command.impl.tour;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;

import by.epam.travelagency.service.entityservice.tour.TourAddService;

import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class TourAddCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map <String, String> entityParameterMap = new HashMap <String,String>();
		
		
		String title = request.getParameter(NamedConstant.PARAM_TOUR_TITLE);
		String begin_date = request.getParameter(NamedConstant.PARAM_TOUR_BEGIN_DATE);
		String end_date = request.getParameter(NamedConstant.PARAM_TOUR_END_DATE);
		String tourTypeId = request.getParameter(NamedConstant.PARAM_TOUR_TOURTYPE);
		String tourStatusId = request.getParameter(NamedConstant.PARAM_TOUR_TOURSTATUS);
		String price = request.getParameter(NamedConstant.PARAM_TOUR_PRICE);
		String description = request.getParameter(NamedConstant.PARAM_TOUR_DESCRIPTION);
		
		String location = request.getParameter(NamedConstant.PARAM_TOUR_LOCATION);
		
			
		
		entityParameterMap.put("title", title);
		entityParameterMap.put("begin_date", begin_date);
		entityParameterMap.put("end_date", end_date);
		entityParameterMap.put("tourTypeId", tourTypeId);
		entityParameterMap.put("tourStatusId", tourStatusId);
		entityParameterMap.put("price", price);
		entityParameterMap.put("description", description);
		entityParameterMap.put("location", location);
		
				
		TourAddService entityAddService = new TourAddService();
				
		
		
		if (entityAddService.entityAdd(entityParameterMap)) {
				
			request.setAttribute("locationaddsuccess",
					MessageManager.getProperty("message.locationaddsuccess"));
			page = ConfigurationManager.getProperty("path.page.tour.show");
				
				
				
				} else {
					request.setAttribute("errorLocationAddMessage",
							MessageManager.getProperty("message.locationadderror")); 
					page = ConfigurationManager.getProperty("path.page.tour.show");
				}
	
		
		return page;
	}

}



