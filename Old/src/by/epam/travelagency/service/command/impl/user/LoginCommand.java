package by.epam.travelagency.service.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.user.LoginService;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class LoginCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {
		User user = null;
		String page = null;
		String login = request.getParameter(NamedConstant.PARAM_USER_LOGIN);
		String password = request
				.getParameter(NamedConstant.PARAM_USER_PASSWORD);
		user = LoginService.checkLogin(login, password);

		if (user != null) {

			//request.setAttribute("user", login);
			

			/////
			
			HttpSession session = request.getSession();
			session.setAttribute(NamedConstant.PARAM_USER_LOGIN, user.getLogin());
			session.setAttribute(NamedConstant.PARAM_USER_ID, user.getId());
			session.setAttribute(NamedConstant.PARAM_USER_ROLE, user.getRole());
			
			
			System.out.println(user.toString());
			
			session.setAttribute("user", user);
			
			
			
			
			/////
			
			logger.debug(session.getAttribute(NamedConstant.PARAM_USER_LOGIN) + " -  login in session");
			logger.debug(session.getAttribute(NamedConstant.PARAM_USER_ROLE)
									
					+ " - role in session");
			logger.debug(session.getAttribute(NamedConstant.PARAM_USER_ID)
					
					+ " - id in session");
			
			// //////
			
			page = ConfigurationManager.getProperty("path.page.main");

		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.login");
		}
		return page;
	}
}
