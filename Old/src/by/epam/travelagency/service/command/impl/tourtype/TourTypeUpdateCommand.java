package by.epam.travelagency.service.command.impl.tourtype;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.location.LocationDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationUpdateServiceImpl;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusUpdateServiceImpl;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeUpdateServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class TourTypeUpdateCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();
		
		String id = request.getParameter(NamedConstant.PARAM_TOURTYPE_ID).trim();
		String name = request.getParameter(NamedConstant.PARAM_TOURTYPE_NAME).trim();
		
		entityParameterMap.put("id", id);
		entityParameterMap.put("name", name);
		
		
			
		page = ConfigurationManager
				.getProperty("path.page.admin.tourtype.update");
		TourType entity = TourTypeUpdateServiceImpl.getInstance().entityUpdate(entityParameterMap);
		
		
		
		
		if (entity != null) {
			request.setAttribute("entity", entity);
			request.setAttribute("tourTypeUpdateSuccessMessage", MessageManager.getProperty("message.tourtype.update.success"));

		}		
	
		else {
	
		request.setAttribute("tourTypeUpdateErrorMessage", MessageManager.getProperty("message.tourtype.update.error"));
			
		}
		
		return page;
	}

}
