package by.epam.travelagency.service.command.impl.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;




import by.epam.travelagency.entity.Order;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.order.OrderDeleteServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class OrderDeleteCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int id = Integer.parseInt(request
				.getParameter(NamedConstant.PARAM_ORDER_ID));

		

		List<Order> entityList = null;
		
		entityList = OrderDeleteServiceImpl.getInstance().entityDelete(id);
		if (entityList != null) {

			request.setAttribute("orderList", entityList);
			page = ConfigurationManager
					.getProperty("path.page.order.show");

		}

		else {
			request.setAttribute("errorOrderStatusDeleteMessage",
					MessageManager.getProperty("message.orderstatusdeleteerror"));
			page = ConfigurationManager
					.getProperty("path.page.order.show");   // доработать
		}

		return page;
	}

}
