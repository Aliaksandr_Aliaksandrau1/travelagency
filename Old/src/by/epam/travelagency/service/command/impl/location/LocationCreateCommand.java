package by.epam.travelagency.service.command.impl.location;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.location.LocationCreateServiceImpl;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusCreateServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class LocationCreateCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();
		
		String country = request.getParameter(NamedConstant.PARAM_LOCATION_COUNTRY).trim();
		String city = request.getParameter(NamedConstant.PARAM_LOCATION_CITY).trim();
		String hotel = request.getParameter(NamedConstant.PARAM_LOCATION_HOTEL).trim();
		String description = request.getParameter(NamedConstant.PARAM_LOCATION_DESCRIPTION).trim();
	
		
		entityParameterMap.put("country", country);
		entityParameterMap.put("city", city);
		entityParameterMap.put("hotel", hotel);
		entityParameterMap.put("description", description);

		
		Location entity = LocationCreateServiceImpl.getInstance().entityCreateNew(entityParameterMap);
		
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.create");

	
		
		if (entity != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("locationCreateSuccessMessage", MessageManager
						.getProperty("message.location.create.success"));
			

			} else {
				request.setAttribute("locationCreateErrorMessage",
						MessageManager
								.getProperty("message.location.create.error"));
				
			}

			return page;
	}

}



