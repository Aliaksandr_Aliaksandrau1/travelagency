package by.epam.travelagency.service.command.impl.tourtype;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.command.ActionCommand;
import resource.ConfigurationManager;
import resource.NamedConstant;


public class TourTypeUpdateGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_TOURTYPE_ID).trim());
		String name = request
				.getParameter(NamedConstant.PARAM_TOURTYPE_NAME).trim();
		
	
		
		TourType entity = new TourType();
		entity.setId(id);
		entity.setName(name);
		request.setAttribute("entity", entity);
	
		
		page = ConfigurationManager.getProperty("path.page.admin.tourtype.update");
		
		
		return page;
	}

}



