package by.epam.travelagency.service.command.impl.tour;


import java.util.List;

import javax.servlet.http.HttpServletRequest;







import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.command.ActionCommand;


import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusShowServiceImpl;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeShowServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;


public class TourAddGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
			
		
		String page = null;
		List <Location> locationList = null;
		List<TourType> tourTypeList = null;
		List<TourStatus> tourStatusList = null;
	
		locationList = LocationShowServiceImpl.getInstance().entityShow();
		tourTypeList = TourTypeShowServiceImpl.getInstance().entityShow();
		tourStatusList = TourStatusShowServiceImpl.getInstance().entityShow();

		if (locationList != null && tourTypeList!=null && tourStatusList!=null) {

			request.setAttribute("locationList", locationList);
			request.setAttribute("tourTypeList", tourTypeList);
			request.setAttribute("tourStatusList", tourStatusList);
				
		 page = ConfigurationManager.getProperty("path.page.tour.add");
	
		
		
		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			 page = ConfigurationManager.getProperty("path.page.tour.add");
		}
		return page;

	
	}

}







	