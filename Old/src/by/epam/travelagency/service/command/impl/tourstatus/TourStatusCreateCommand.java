package by.epam.travelagency.service.command.impl.tourstatus;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;








import org.apache.log4j.Logger;

import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusCreateServiceImpl;

import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class TourStatusCreateCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map <String, String> entityParameterMap = new HashMap <String,String>();
	
		String name = request.getParameter(NamedConstant.PARAM_TOURSTATUS_NAME).trim();

		entityParameterMap.put("name", name);
					
				
		
				
		TourStatus entity = TourStatusCreateServiceImpl.getInstance()
				.entityCreateNew(entityParameterMap);
		
		page = ConfigurationManager.getProperty("path.page.admin.tourstatus.create");
			
		
		if (entity != null) {
				
		request.setAttribute("entity", entity);
		request.setAttribute("tourStatusCreateSuccessMessage",
					MessageManager.getProperty("message.tourstatus.create.success"));
			
		
		} else {
		request.setAttribute("tourStatusCreateErrorMessage",
							MessageManager.getProperty("message.tourstatus.create.error")); 
		
		}
		
		return page;
	}

}



