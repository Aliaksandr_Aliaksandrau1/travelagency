package by.epam.travelagency.service.command.impl.user;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;

import resource.ConfigurationManager;


public class LoginGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		page = ConfigurationManager.getProperty("path.page.login");
		
		
		return page;
	}

}



