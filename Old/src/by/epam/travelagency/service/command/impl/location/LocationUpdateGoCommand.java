package by.epam.travelagency.service.command.impl.location;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Location;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.location.LocationCreateServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationUpdateGoServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;


public class LocationUpdateGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
					
		String page = null;
		
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_LOCATION_ID));
		Location entity = LocationUpdateGoServiceImpl.getInstance().entityToUpdate(id);
		
	
		
			
		page = ConfigurationManager.getProperty("path.page.touragent.location.update");
		if (entity != null) {
			
			request.setAttribute("entity", entity);
			request.setAttribute("locationFindSuccessMessage", MessageManager
						.getProperty("message.location.find.success"));
			

			} else {
				request.setAttribute("locationFindErrorMessage",
						MessageManager
								.getProperty("message.location.find.error"));
				
			}

		return page;
	}

}



