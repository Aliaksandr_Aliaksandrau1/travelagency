package by.epam.travelagency.service.command.impl.tourstatus;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.TourStatus;

import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusUpdateServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class TourStatusUpdateCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();
		
		String id = request.getParameter(NamedConstant.PARAM_TOURSTATUS_ID).trim();
		String name = request.getParameter(NamedConstant.PARAM_TOURSTATUS_NAME).trim();
		
		entityParameterMap.put("id", id);
		entityParameterMap.put("name", name);
		
		
			
		
		TourStatus entity = TourStatusUpdateServiceImpl.getInstance().entityUpdate(entityParameterMap);
		
		
		if (entity != null) {


			request.setAttribute("entity", entity);
			request.setAttribute("tourStatusUpdateSuccessMessage", MessageManager.getProperty("message.tourstatus.update.success"));
			page = ConfigurationManager
					.getProperty("path.page.admin.tourstatus.update");
		}		
	
		else {
	
		request.setAttribute("tourStatusUpdateErrorMessage", MessageManager.getProperty("message.tourstatus.update.error"));
			page = ConfigurationManager
					.getProperty("path.page.admin.tourstatus.update");
		}

		return page;
	}

}
