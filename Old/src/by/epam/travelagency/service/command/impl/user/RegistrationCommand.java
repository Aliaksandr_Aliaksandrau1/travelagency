package by.epam.travelagency.service.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.user.RegistrationService;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;

public class RegistrationCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		
		logger.debug("Registration command"); // ������
		
		
		String page = null;
		String login = request.getParameter(NamedConstant.PARAM_USER_LOGIN);
		String pass = request.getParameter(NamedConstant.PARAM_USER_PASSWORD);
		String role = request.getParameter(NamedConstant.PARAM_USER_ROLE);
		String name = request.getParameter(NamedConstant.PARAM_USER_NAME);
		String surname = request.getParameter(NamedConstant.PARAM_USER_SURNAME);
		String telnumber = request.getParameter(NamedConstant.PARAM_USER_TELNUMBER);
		String email = request.getParameter(NamedConstant.PARAM_USER_EMAIL);
		double discount = Double.parseDouble(request.getParameter(NamedConstant.PARAM_USER_DISCOUNT));
		
				
		RegistrationService regLogic = new RegistrationService ();
				
				if (regLogic.userRegistration(login, pass, role, name, surname, telnumber, email, discount)) {
					
					
					
					//request.setAttribute("user", login);
					
					page = ConfigurationManager.getProperty("path.page.main");
					////

					HttpSession session = request.getSession();
					session.setAttribute(NamedConstant.PARAM_USER_LOGIN, login);
					//session.setAttribute(NamedConstant.PARAM_USER_ID, user.getId());
					session.setAttribute(NamedConstant.PARAM_USER_ROLE, role);
					
					
					
					//session.getAttribute(NamedConstant.PARAM_USER_ROLE);
					
					/////
					//logger.debug(session.getAttribute("login") + " -  login in session + registration");
					
				} else {
					request.setAttribute("errorLoginPassMessage",
							MessageManager.getProperty("message.registrationerror")); 
					page = ConfigurationManager.getProperty("path.page.registration");
				}
		
		
		
		
		
		
		
		return page;
	}

}



