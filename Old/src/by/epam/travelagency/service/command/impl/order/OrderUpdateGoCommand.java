package by.epam.travelagency.service.command.impl.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.command.ActionCommand;
import by.epam.travelagency.service.entityservice.order.OrderUpdateGoService;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusShowServiceImpl;
import resource.ConfigurationManager;
import resource.MessageManager;
import resource.NamedConstant;


public class OrderUpdateGoCommand implements ActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		Order entity = null;
		List<OrderStatus> orderStatusList = null;
		
		
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_ORDER_ID));
				System.out.println(id);
	
		
		entity = OrderUpdateGoService.getInstance().entityUpdate(id);
		orderStatusList = OrderStatusShowServiceImpl.getInstance().entityShow();
		
		System.out.println(entity);
		
		if (entity != null && orderStatusList != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("orderStatusList", orderStatusList);
			
			page = ConfigurationManager.getProperty("path.page.order.update");

		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.order.update"); // доработать
		}
		
		return page;
	}

}




