package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import resource.NamedConstant;
import by.epam.travelagency.dao.EntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.OrderStatus;

public class LocationDAOImpl implements EntityDAO<Location> {
	private final static Logger logger = Logger.getRootLogger();

	private LocationDAOImpl() {
	}

	private final static LocationDAOImpl instance = new LocationDAOImpl();

	public static LocationDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<Location> findAll() {
		List<Location> locationList = new ArrayList<Location>();
		Location entity = null;
		try {
			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION_FIND_ALL);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				entity = new Location();

				entity.setId(resultSet.getInt(NamedConstant.DB_LOCATION_ID));
				entity.setCountry(resultSet
						.getString(NamedConstant.DB_LOCATION_COUNTRY));
				entity.setCity(resultSet
						.getString(NamedConstant.DB_LOCATION_CITY));
				entity.setHotel(resultSet
						.getString(NamedConstant.DB_LOCATION_HOTEL));
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_LOCATION_DESCRIPTION));

				locationList.add(entity);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return locationList;
	}

	@Override
	public boolean create(Location location) {
		logger.debug("LocationDaoImpl.create() with preparedStatement "); // ������!!!

		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION_CREATE);

			statement.setString(1, location.getCountry());
			statement.setString(2, location.getCity());
			statement.setString(3, location.getHotel());
			statement.setString(4, location.getDescription());
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isCreated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public Location findEntityById(int id) {
		Location entity = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION_FIND_BY_ID);
			statement.setInt(1, id);
			
			resultSet = statement.executeQuery();
	
			while (resultSet.next()) {
				entity = new Location();
				entity.setId(resultSet.getInt(NamedConstant.DB_LOCATION_ID));
				entity.setCountry(resultSet
						.getString(NamedConstant.DB_LOCATION_COUNTRY));
				entity.setCity(resultSet
						.getString(NamedConstant.DB_LOCATION_CITY));
				entity.setHotel(resultSet
						.getString(NamedConstant.DB_LOCATION_HOTEL));
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_LOCATION_DESCRIPTION));

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entity;
	}

	@Override
	public boolean delete (int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(Location location) {
		boolean isUpdated = false;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION_UPDATE);

			statement.setString(1, location.getCountry());
			statement.setString(2, location.getCity());
			statement.setString(3, location.getHotel());
			statement.setString(4, location.getDescription());
			statement.setInt(5, location.getId());
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
