package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import resource.NamedConstant;
import by.epam.travelagency.dao.EntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.entity.User;



public class OrderDAOImpl implements EntityDAO<Order> {
	private final static Logger logger = Logger.getRootLogger();
	private OrderDAOImpl() {
	}
	private final static OrderDAOImpl instance = new OrderDAOImpl();
	public static OrderDAOImpl getInstance() {
		return instance;
	}
	@Override
	public List<Order> findAll() {
		
		List<Order> entityList = null;
		Order order = null;
		User user = null;
		OrderStatus orderStatus = null;
		Tour tour = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDER_FIND_ALL);
			resultSet = statement.executeQuery();
			entityList = new ArrayList<Order>();

			while (resultSet.next()) {
				order = new Order();
				user = new User();
				orderStatus = new OrderStatus();
				tour = new Tour();

				user.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
				user.setLogin(resultSet.getString(NamedConstant.DB_USER_LOGIN));
				user.setName(resultSet.getString(NamedConstant.DB_USER_NAME));
				user.setSurname(resultSet.getString(NamedConstant.DB_USER_SURNAME));
				user.setTelephoneNumber(resultSet.getString(NamedConstant.DB_USER_TELNUMBER));
				user.setEmail(resultSet.getString(NamedConstant.DB_USER_EMAIL));
				user.setDiscount(resultSet.getDouble(NamedConstant.DB_USER_DISCOUNT));
				
				orderStatus.setId(resultSet.getInt(NamedConstant.DB_ORDERSTATUS_ID));
				orderStatus.setName(resultSet.getString(NamedConstant.DB_ORDERSTATUS_NAME));
				
				tour.setId(resultSet.getInt(NamedConstant.DB_TOUR_ID));
				tour.setTitle(resultSet.getString(NamedConstant.DB_TOUR_TITLE));
				tour.setBeginDate(resultSet.getDate(NamedConstant.DB_TOUR_BEGIN_DATE));
				tour.setEndDate(resultSet.getDate(NamedConstant.DB_TOUR_END_DATE));
				tour.setPrice(resultSet.getDouble(NamedConstant.DB_TOUR_PRICE));
				tour.setDescription(resultSet.getString(NamedConstant.DB_TOUR_DESCRIPTION));
				
				
				order.setId(resultSet.getInt(NamedConstant.DB_ORDER_ID));
				order.setUser(user);
				order.setOrderStatus(orderStatus);
				order.setTour(tour);
									
				entityList.add(order);
					

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;
	
	}
	@Override
	public boolean create(Order entity) {
		
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			PreparedStatement statement = connection
					.prepareStatement(Query.ORDER_CREATE);
		
			statement.setInt (1, entity.getUser().getId());
			statement.setInt (2, entity.getTour().getId());
			statement.setInt (3, entity.getOrderStatus().getId());
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			
			isCreated = true;
	
			
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}
		
		
		
		
		return isCreated;
	}
	@Override
	public Order findEntityById(int id) {
		
		Order order = null;
		User user = null;
		OrderStatus orderStatus = null;
		Tour tour = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDER_FIND_BY_ID);
			
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			

			while (resultSet.next()) {
				order = new Order();
				user = new User();
				orderStatus = new OrderStatus();
				tour = new Tour();

				user.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
				user.setLogin(resultSet.getString(NamedConstant.DB_USER_LOGIN));
				user.setName(resultSet.getString(NamedConstant.DB_USER_NAME));
				user.setSurname(resultSet.getString(NamedConstant.DB_USER_SURNAME));
				user.setTelephoneNumber(resultSet.getString(NamedConstant.DB_USER_TELNUMBER));
				user.setEmail(resultSet.getString(NamedConstant.DB_USER_EMAIL));
				user.setDiscount(resultSet.getDouble(NamedConstant.DB_USER_DISCOUNT));
				
				orderStatus.setId(resultSet.getInt(NamedConstant.DB_ORDERSTATUS_ID));
				orderStatus.setName(resultSet.getString(NamedConstant.DB_ORDERSTATUS_NAME));
				
				tour.setId(resultSet.getInt(NamedConstant.DB_TOUR_ID));
				tour.setTitle(resultSet.getString(NamedConstant.DB_TOUR_TITLE));
				tour.setBeginDate(resultSet.getDate(NamedConstant.DB_TOUR_BEGIN_DATE));
				tour.setEndDate(resultSet.getDate(NamedConstant.DB_TOUR_END_DATE));
				tour.setPrice(resultSet.getDouble(NamedConstant.DB_TOUR_PRICE));
				tour.setDescription(resultSet.getString(NamedConstant.DB_TOUR_DESCRIPTION));
				
				
				order.setId(resultSet.getInt(NamedConstant.DB_ORDER_ID));
				order.setUser(user);
				order.setOrderStatus(orderStatus);
				order.setTour(tour);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		
		return order;
	}
	
	
	
	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDER_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}
	@Override
	public boolean update(Order entity) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
