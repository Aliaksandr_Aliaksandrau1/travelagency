package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import resource.NamedConstant;
import by.epam.travelagency.dao.EntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.TourType;

public class TourTypeDAOImpl implements EntityDAO<TourType> {
	private final static Logger logger = Logger.getRootLogger();

	private TourTypeDAOImpl() {

	}

	private final static TourTypeDAOImpl instance = new TourTypeDAOImpl();

	public static TourTypeDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<TourType> findAll() {
		List<TourType> tourTypeList = null;
		TourType tourType = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURTYPE_FIND_ALL);
			resultSet = statement.executeQuery();

			tourTypeList = new ArrayList<TourType>();

			while (resultSet.next()) {
				tourType = new TourType();

				tourType.setId(resultSet.getInt(NamedConstant.DB_TOURTYPE_ID));
				tourType.setName(resultSet
						.getString(NamedConstant.DB_TOURTYPE_NAME));
				tourTypeList.add(tourType);
								
					}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return tourTypeList;

	}

	@Override
	public boolean create(TourType tourType) {
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURTYPE_CREATE);

			statement.setString(1, tourType.getName());
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);

			isCreated = true;
			
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public TourType findEntityById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int id) {
	

		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURTYPE_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(TourType entity) {
		

		boolean isUpdated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURTYPE_UPDATE);

			statement.setString(1, entity.getName());
			statement.setInt(2, entity.getId());
			
						
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
