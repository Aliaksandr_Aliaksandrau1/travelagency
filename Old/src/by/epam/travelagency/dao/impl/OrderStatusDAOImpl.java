package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import resource.NamedConstant;
import by.epam.travelagency.dao.EntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.OrderStatus;

public class OrderStatusDAOImpl implements EntityDAO <OrderStatus> {
	private final static Logger logger = Logger.getRootLogger();

	private OrderStatusDAOImpl() {

	}

	private final static OrderStatusDAOImpl instance = new OrderStatusDAOImpl();

	public static OrderStatusDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<OrderStatus> findAll() {
		List<OrderStatus> entityList = null;
		OrderStatus entity = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDERSTATUS_FIND_ALL);
			resultSet = statement.executeQuery();

			entityList = new ArrayList<OrderStatus>();

			while (resultSet.next()) {
				entity = new OrderStatus();

				entity.setId(resultSet.getInt(NamedConstant.DB_ORDERSTATUS_ID));
				entity.setName(resultSet
						.getString(NamedConstant.DB_ORDERSTATUS_NAME));
				entityList.add(entity);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;

	}

	@Override
	public boolean create(OrderStatus entity) {

		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDERSTATUS_CREATE);

			statement.setString(1, entity.getName());

			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isCreated = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public OrderStatus findEntityById(int id) {
		OrderStatus entity = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDERSTATUS_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			
			
			
			while (resultSet.next()) {
				entity = new OrderStatus();
				entity.setId(resultSet.getInt(NamedConstant.DB_ORDERSTATUS_ID));
				entity.setName(resultSet
						.getString(NamedConstant.DB_ORDERSTATUS_NAME));

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entity;
	}

	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDERSTATUS_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(OrderStatus entity) {
		boolean isUpdated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.ORDERSTATUS_UPDATE);

			statement.setString(1, entity.getName());
			statement.setInt(2, entity.getId());

			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
