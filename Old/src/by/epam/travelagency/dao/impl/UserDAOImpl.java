package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import resource.NamedConstant;
import by.epam.travelagency.dao.EntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.UserDAO;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.User;

public class UserDAOImpl implements UserDAO, EntityDAO<User> {
	private final static Logger logger = Logger.getRootLogger();

	private UserDAOImpl() {
	}

	private final static UserDAOImpl instance = new UserDAOImpl();

	public static UserDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<User> findAll() {
		List<User> userList = new ArrayList<User>();

		try {
			logger.debug("UserDaoImpl.findAll() with preparedStatement "); // ������!!!

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_ALL);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				userList.add(new User(resultSet.getInt("usr_id"), resultSet
						.getString("usr_login"), resultSet
						.getString("usr_password"), resultSet
						.getString("usr_role"),
						resultSet.getString("usr_name"), resultSet
								.getString("usr_surname"), resultSet
								.getString("usr_telnumber"), resultSet
								.getString("usr_email"), resultSet
								.getDouble("usr_discount")));

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return userList;
	}

	@Override
	public boolean create(User user) {
		logger.debug("UserDaoImpl.create() with preparedStatement "); // ������!!!
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_CREATE_ACCOUNT);

			// statement.setInt(1, user.getId());
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getRole());
			statement.setString(4, user.getName());
			statement.setString(5, user.getSurname());
			statement.setString(6, user.getTelephoneNumber());
			statement.setString(7, user.getEmail());
			statement.setDouble(8, user.getDiscount());
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public User findEntityById(int id) {
		User user = null;
		try {
			logger.debug("UserDaoImpl.findEntityById (int id)"); // ������!!!

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_BY_ID);

			statement.setInt(1, id);

			ResultSet resultSet = statement.executeQuery();

			if (!resultSet.wasNull()) { // ����������� ����������
										// ��������!!!!!!!!!!!!!!

				while (resultSet.next()) {

					user = new User();
					user.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
					user.setLogin(resultSet
							.getString(NamedConstant.DB_USER_LOGIN));
					user.setPassword(resultSet
							.getString(NamedConstant.DB_USER_PASSWORD));
					user.setRole(resultSet
							.getString(NamedConstant.DB_USER_ROLE));
					user.setName(resultSet
							.getString(NamedConstant.DB_USER_NAME));
					user.setSurname(resultSet
							.getString(NamedConstant.DB_USER_SURNAME));
					user.setTelephoneNumber(resultSet
							.getString(NamedConstant.DB_USER_TELNUMBER));
					user.setEmail(resultSet
							.getString(NamedConstant.DB_USER_EMAIL));
					user.setDiscount(resultSet
							.getDouble(NamedConstant.DB_USER_DISCOUNT));

				}

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return user;

	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(User entity) {
		// TODO Auto-generated method stub
		return false;
	}


	// @Override
	public String isLoginOccupied(String login) {
		String checkLogin = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_BY_LOGIN);

			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {

				checkLogin = resultSet.getString(1);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return checkLogin;
	}

	@Override
	public User checkLogin(String login, String password) {
		User user = null;
		try {
			
			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_CHECK_LOGIN);

			statement.setString(1, login);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();

			if (!resultSet.wasNull()) { // ����������� ����������
										// ��������!!!!!!!!!!!!!!

				while (resultSet.next()) {

					user = new User();
					user.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
					user.setLogin(resultSet
							.getString(NamedConstant.DB_USER_LOGIN));
					
					
					user.setPassword(resultSet.getString(NamedConstant.DB_USER_PASSWORD));
					
					
					user.setRole(resultSet
							.getString(NamedConstant.DB_USER_ROLE));
					user.setName(resultSet
							.getString(NamedConstant.DB_USER_NAME));
					user.setSurname(resultSet
							.getString(NamedConstant.DB_USER_SURNAME));
					user.setTelephoneNumber(resultSet
							.getString(NamedConstant.DB_USER_TELNUMBER));
					user.setEmail(resultSet
							.getString(NamedConstant.DB_USER_EMAIL));
					user.setDiscount(resultSet
							.getDouble(NamedConstant.DB_USER_DISCOUNT));

					System.out.println("user from db" + user.toString()); // ������!!!

				}

				// user = userFromDB(resultSet);

			}

			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return user;
	}

}
