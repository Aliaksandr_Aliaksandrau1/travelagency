package by.epam.travelagency.dao;

import java.util.List;

import by.epam.travelagency.entity.Entity;

public interface EntityDAO<T extends Entity> {

	public List<T> findAll();

	public boolean create(T entity);

	public T findEntityById(int id);

	public boolean delete(int id);

	/*
	 * public boolean delete(T entity);
	 */

	public boolean update(T entity);

}
