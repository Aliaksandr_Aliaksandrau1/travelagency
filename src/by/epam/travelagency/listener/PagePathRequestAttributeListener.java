package by.epam.travelagency.listener;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

import by.epam.travelagency.service.resource.ConfigurationManager;

/**
 * Application Lifecycle Listener implementation class PagePathRequestAttributeListener
 *
 */
@WebListener
public class PagePathRequestAttributeListener implements ServletRequestAttributeListener {

    /**
     * Default constructor. 
     */
    public PagePathRequestAttributeListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletRequestAttributeListener#attributeRemoved(ServletRequestAttributeEvent)
     */
    public void attributeRemoved(ServletRequestAttributeEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletRequestAttributeListener#attributeAdded(ServletRequestAttributeEvent)
     */
    public void attributeAdded(ServletRequestAttributeEvent event)  { 
        
    	HttpServletRequest request = (HttpServletRequest)event.getServletRequest();
    	
    		//request.setAttribute("path_page_start_page", ConfigurationManager.getProperty("path.page.start_page"));
    		
    		request.setAttribute("path_page_i18n", ConfigurationManager.getProperty("path.page.i18n"));
    		request.setAttribute("path_page_header", ConfigurationManager.getProperty("path.page.header"));
    		request.setAttribute("path_page_footer", ConfigurationManager.getProperty("path.page.footer"));
    		request.setAttribute("path_page_tour_show", ConfigurationManager.getProperty("path.page.tour.show"));
    		
    		request.setAttribute("path_style_css", ConfigurationManager.getProperty("path.page.style_css"));
    		
    	
    }

	/**
     * @see ServletRequestAttributeListener#attributeReplaced(ServletRequestAttributeEvent)
     */
    public void attributeReplaced(ServletRequestAttributeEvent arg0)  { 
         // TODO Auto-generated method stub
    }
	
}
