package by.epam.travelagency.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import by.epam.travelagency.service.resource.NamedConstant;

/**
 * Application Lifecycle Listener implementation class UserRoleListener
 *
 */
@WebListener
public class UserRoleListener implements HttpSessionAttributeListener {

    /**
     * Default constructor. 
     */
    public UserRoleListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
     */
    public void attributeAdded(HttpSessionBindingEvent event)  { 
         // TODO Auto-generated method stub
    	event.getSession().setAttribute(NamedConstant.PARAM_USER_ROLE,  NamedConstant.PARAM_USER_ROLE_BY_DEFAULT);
    	event.getSession().setAttribute(NamedConstant.PARAM_SESSION_LOCAL,  NamedConstant.PARAM_SESSION_LOCAL_BY_DEFAULT);
    }

	/**
     * @see HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
     */
    public void attributeRemoved(HttpSessionBindingEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent)
     */
    public void attributeReplaced(HttpSessionBindingEvent arg0)  { 
         // TODO Auto-generated method stub
    }
	
}
