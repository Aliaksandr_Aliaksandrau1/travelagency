package by.epam.travelagency.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;

/**
 * Application Lifecycle Listener implementation class DatabaseConnectionListener
 *
 */
@WebListener
public class DatabaseConnectionListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public DatabaseConnectionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent event)  { 
        
    	ConnectionPool.getInstance().dispose();   // ����� ��???
    	
    	
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event)  { 
        	 
    		try {
    			ConnectionPool.getInstance().initPoolData();
    		} catch (ConnectionPoolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	 
    	 
    }
	
}
