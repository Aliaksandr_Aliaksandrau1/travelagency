package by.epam.travelagency.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.ActionFactory;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.ValidationRegExManager;

/**
 * HttpServlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private final static Logger logger = Logger.getRootLogger();

	public Controller() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String page = null;

		IActionCommand command = ActionFactory.getInstance().defineCommand(
				request);

		try {
							
			page = command.execute(request);
		} catch (CommandException e) {
			page = ConfigurationManager.getProperty("path.page.error");
			logger.error("CommandException: " + e);
			
		
		}
		 catch (Exception e) {
			page = ConfigurationManager.getProperty("path.page.error");
			logger.error("Exception: "+ e);
			
				
		}
		
		
		logger.debug("page in controller = " + page);
			
		
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher(page);

			dispatcher.forward(request, response);
		} else {

			page = ConfigurationManager.getProperty("path.page.index");
			request.getSession().setAttribute("nullPage",
					MessageManager.getProperty("message.nullpage"));
			response.sendRedirect(request.getContextPath() + page);
		}
	}

}
