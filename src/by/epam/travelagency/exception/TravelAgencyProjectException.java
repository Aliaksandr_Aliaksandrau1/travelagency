package by.epam.travelagency.exception;

public class TravelAgencyProjectException extends Exception {
	private static final long serialVersionUID = 1L;

	public TravelAgencyProjectException() {
	}

	public TravelAgencyProjectException(String message) {
		super(message);
	}

	public TravelAgencyProjectException(Throwable cause) {
		super(cause);
	}

	public TravelAgencyProjectException(String message, Throwable cause) {
		super(message, cause);
	}

}
