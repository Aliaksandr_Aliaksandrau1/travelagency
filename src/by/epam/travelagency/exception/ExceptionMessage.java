package by.epam.travelagency.exception;

public class ExceptionMessage {
	
	public static final String DAO_DB_ERROR = "Error in connection with DB";
	
	// Command messages

	
	public static final String COMMAND_USER_SHOW = "Exception in UserShowCommand: ";
	public static final String COMMAND_USER_CREATE = "Exception in UserCreateCommand: ";
	public static final String COMMAND_USER_CREATE_GO = "Exception in UserCreateGoCommand: ";
	public static final String COMMAND_USER_DELETE = "Exception in UserDeleteCommand: ";
	public static final String COMMAND_USER_UPDATE = "Exception in UserUpdateCommand: ";
	public static final String COMMAND_USER_UPDATE_GO = "Exception in UserUpdateGoCommand: ";
	
	
	
	
	public static final String COMMAND_LOCATION_SHOW = "Exception in LocationShowCommand: ";
	public static final String COMMAND_LOCATION_CREATE = "Exception in LocationCreateCommand: ";
	public static final String COMMAND_LOCATION_CREATE_GO = "Exception in LocationCreateGoCommand: ";
	public static final String COMMAND_LOCATION_DELETE = "Exception in LocationDeleteCommand: ";
	public static final String COMMAND_LOCATION_UPDATE = "Exception in LocationUpdateCommand: ";
	public static final String COMMAND_LOCATION_UPDATE_GO = "Exception in LocationUpdateGoCommand: ";
	
	
	
	
	//entity service exception messages
	
	public static final String ENTITYSERVICE_USER_SHOW = "Exception in UserShowserviceImpl: ";
	public static final String ENTITYSERVICE_USER_CREATE = "Exception in UserCreateserviceImpl: ";
	public static final String ENTITYSERVICE_USER_DELETE = "Exception in UserDeleteServiceImpl: ";
	public static final String ENTITYSERVICE_USER_UPDATE = "Exception in UserUpdateServiceImpl: ";
	public static final String ENTITYSERVICE_USER_UPDATE_GO = "Exception in UserUpdateGoServiceImpl: ";
	
	
	
	public static final String ENTITYSERVICE_LOCATION_SHOW = "Exception in LocationShowserviceImpl: ";
	public static final String ENTITYSERVICE_LOCATION_CREATE = "Exception in LocationCreateserviceImpl: ";
	public static final String ENTITYSERVICE_LOCATION_DELETE = "Exception in LocationDeleteServiceImpl: ";
	public static final String ENTITYSERVICE_LOCATION_UPDATE = "Exception in LocationUpdateServiceImpl: ";
	public static final String ENTITYSERVICE_LOCATION_UPDATE_GO = "Exception in LocationUpdateGoServiceImpl: ";
	
	
	
	
	public static final String DAO_DATABASE_CONNECTION = "Problem in connection with data base: ";
	public static final String DAO_DATABASE_CLOSE_CONNECTION = "Problem with closing the connection: ";
	
		
}
