package by.epam.travelagency.command;

import javax.servlet.http.HttpServletRequest;





import org.apache.log4j.Logger;

import by.epam.travelagency.command.impl.EmptyCommand;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class ActionFactory {
	private ActionFactory() { 
	}

	private final static ActionFactory instance = new ActionFactory();
	private final static Logger logger = Logger.getRootLogger();
	
	public static ActionFactory getInstance() {
		return instance;
	}
	
	public IActionCommand defineCommand(HttpServletRequest request) {
	
		IActionCommand current = new EmptyCommand();  // �������� ����� ����� enum
		
		String action = request.getParameter("command");
	
	
		request.setAttribute("path_page_start_page", ConfigurationManager.getProperty("path.page.start_page"));
		
		logger.debug("Command name = " + action); // ������
		
		if (action == null || action.isEmpty()) {
						
			return current;
		}
		
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute("wrongAction",
					action + MessageManager.getProperty("message.wrongaction"));
		}
		return current;
	}
}
