package by.epam.travelagency.command;

import by.epam.travelagency.command.impl.*;
import by.epam.travelagency.command.impl.location.*;
import by.epam.travelagency.command.impl.order.*;
import by.epam.travelagency.command.impl.orderstatus.*;
import by.epam.travelagency.command.impl.tour.*;
import by.epam.travelagency.command.impl.tourstatus.*;
import by.epam.travelagency.command.impl.tourtype.*;
import by.epam.travelagency.command.impl.user.*;


public enum CommandEnum {
	EMPTY {
		{
			this.command = new EmptyCommand();
		}
	},
	LOGIN {
		{
			this.command = new UserLoginCommand();
		}
	},
	LOGIN_GO {
		{
			this.command = new UserLoginGoCommand();
		}
	},
	LOGOUT {
		{
			this.command = new UserLogoutCommand();
		}
	},

	USER_REGISTRATION {
		{
			this.command = new UserRegistrationCommand();
		}
	},
	REGISTRATION_GO {
		{
			this.command = new UserRegistrationGoCommand();
		}
	}, 
	USER_SHOW {
		{
			this.command = new UserShowCommand();
		}
	},
	USER_DELETE {
		{
			this.command = new UserDeleteCommand();
		}
	},
	USER_CREATE {
		{
			this.command = new UserCreateCommand();
		}
	},
	USER_CREATE_GO {
		{
			this.command = new UserCreateGoCommand();
		}
	},
	USER_UPDATE_GO {
		{
			this.command = new UserUpdateGoCommand();
		}
	},
	USER_UPDATE {
		{
			this.command = new UserUpdateCommand();
		}
	},
	
	
	LOCATION_SHOW {
		{
			this.command = new LocationShowCommand();
		}
	},
	LOCATION_CREATE {
		{
			this.command = new LocationCreateCommand();
		}
	},
	LOCATION_CREATE_GO {
		{
			this.command = new LocationCreateGoCommand();
		}
	},
	LOCATION_DELETE {
		{
			this.command = new LocationDeleteCommand();
		}
	},
	LOCATION_UPDATE_GO {
		{
			this.command = new LocationUpdateGoCommand();
		}
	},
	LOCATION_UPDATE {
		{
			this.command = new LocationUpdateCommand();
		}
	},
	TOURTYPE_SHOW {
		{
			this.command = new TourTypeShowCommand();
		}
	},
	TOURTYPE_DELETE {
		{
			this.command = new TourTypeDeleteCommand();
		}
	},
	TOURTYPE_UPDATE {
		{
			this.command = new TourTypeUpdateCommand();
		}
	},
	TOURTYPE_UPDATE_GO {
		{
			this.command = new TourTypeUpdateGoCommand();
		}
	},
	TOURTYPE_CREATE {
		{
			this.command = new TourTypeCreateCommand();
		}
	},
	TOURTYPE_CREATE_GO {
		{
			this.command = new TourTypeCreateGoCommand();
		}
	},
	TOURSTATUS_SHOW {
		{
			this.command = new TourStatusShowCommand();
		}
	},
	TOURSTATUS_DELETE {
		{
			this.command = new TourStatusDeleteCommand();
		}
	},
	TOURSTATUS_UPDATE {
		{
			this.command = new TourStatusUpdateCommand();
		}
	},
	TOURSTATUS_UPDATE_GO {
		{
			this.command = new TourStatusUpdateGoCommand();
		}
	},
	TOURSTATUS_CREATE {
		{
			this.command = new TourStatusCreateCommand();
		}
	},
	TOURSTATUS_CREATE_GO {
		{
			this.command = new TourStatusCreateGoCommand();
		}
	},
	ORDERSTATUS_SHOW {
		{
			this.command = new OrderStatusShowCommand();
		}
	},
	ORDERSTATUS_CREATE {
		{
			this.command = new OrderStatusCreateCommand();
		}
	},
	ORDERSTATUS_CREATE_GO {
		{
			this.command = new OrderStatusCreateGoCommand();
		}
	},
	ORDERSTATUS_DELETE {
		{
			this.command = new OrderStatusDeleteCommand();
		}
	},
	ORDERSTATUS_UPDATE {
		{
			this.command = new OrderStatusUpdateCommand();
		}
	},
	ORDERSTATUS_UPDATE_GO {
		{
			this.command = new OrderStatusUpdateGoCommand();
		}
	},
	
	TOUR_SHOW {
		{
			this.command = new TourShowCommand();
		}
	},
	TOUR_ADD {
		{
			this.command = new TourAddCommand();
		}
	},
	TOUR_ADD_GO {
		{
			this.command = new TourAddGoCommand();
		}
	},
	TOUR_DELETE {
		{
			this.command = new TourDeleteCommand();
		}
	},
	TOUR_UPDATE_GO {
		{
			this.command = new TourUpdateGoCommand();
		}
	},
	TOUR_UPDATE {
		{
			this.command = new TourUpdateCommand();
		}
	},
	ORDER_CREATE {
		{
			this.command = new OrderCreateCommand();
		}
	},
	ORDER_SHOW {
		{
			this.command = new OrderShowCommand();
		}
	},
	ORDER_DELETE {
		{
			this.command = new OrderDeleteCommand();
		}
	},
	ORDER_UPDATE {
		{
			this.command = new OrderUpdateCommand();
		}
	},
	ORDER_UPDATE_GO {
		{
			this.command = new OrderUpdateGoCommand();
		}
	},
	I18N {
		{
			this.command = new I18nCommand();
		}
	};  
	
	IActionCommand command;

	public IActionCommand getCurrentCommand() {
		return command;
	}

}
