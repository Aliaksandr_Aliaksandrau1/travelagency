package by.epam.travelagency.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.travelagency.command.exception.CommandException;

public interface IActionCommand  {
	String execute(HttpServletRequest request) throws CommandException;
}
