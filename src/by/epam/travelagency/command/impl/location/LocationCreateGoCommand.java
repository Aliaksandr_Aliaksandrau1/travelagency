package by.epam.travelagency.command.impl.location;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.service.resource.ConfigurationManager;

public class LocationCreateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) throws CommandException  {

		String page = ConfigurationManager
				.getProperty("path.page.touragent.location.create");

		return page;
	}

}
