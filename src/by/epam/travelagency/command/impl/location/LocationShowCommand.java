package by.epam.travelagency.command.impl.location;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class LocationShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute (HttpServletRequest request) throws CommandException {

		List<Location> entityList = new ArrayList<Location>();
		String page = null;

		try {
			
			entityList = LocationShowServiceImpl.getInstance().entityShow();
			
			
		} catch (ServiceException e) {
			throw new CommandException( ExceptionMessage.COMMAND_LOCATION_SHOW + e, e);
			
		}
		

		if (entityList != null) {

			request.setAttribute("locationList", entityList);
			page = ConfigurationManager
					.getProperty("path.page.touragent.location.show");

		} else {
			request.setAttribute("locationShowErrorMessage",
					MessageManager.getProperty("message.location.show.error"));
			page = ConfigurationManager
					.getProperty("path.page.touragent.location.show");

		}
		return page;
	}
}
