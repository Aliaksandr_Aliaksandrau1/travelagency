package by.epam.travelagency.command.impl.location;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.location.LocationDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class LocationDeleteCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String page = null;
		List<Location> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request.getParameter(
				NamedConstant.PARAM_LOCATION_ID).trim());

		try {
			isDeleted = LocationDeleteServiceImpl.getInstance().entityDeleteNew(id);
		} catch (ServiceException e) {
			throw new CommandException( ExceptionMessage.COMMAND_LOCATION_DELETE + e, e);
		}
		try {
			entityList = LocationShowServiceImpl.getInstance().entityShow();
		} catch (ServiceException e) {
			throw new CommandException( ExceptionMessage.COMMAND_LOCATION_SHOW + e, e);
		}

		
		request.setAttribute("locationList", entityList);
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.show");

		if (isDeleted) {

			request.setAttribute("locationDeleteSuccessMessage", MessageManager
					.getProperty("message.location.delete.success"));

		}

		else {

			request.setAttribute("locationStatusDeleteErrorMessage",
					MessageManager.getProperty("message.location.delete.error"));
		}

		return page;
	}

}
