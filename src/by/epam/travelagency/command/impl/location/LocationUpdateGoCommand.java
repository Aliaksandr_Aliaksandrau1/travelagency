package by.epam.travelagency.command.impl.location;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.location.LocationCreateServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationUpdateGoServiceImpl;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class LocationUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
					
		String page = null;
		Location entity = null;
		
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_LOCATION_ID));
		try {
			entity = LocationUpdateGoServiceImpl.getInstance().entityToUpdate(id);
		} catch (ServiceException e) {
			throw new CommandException( ExceptionMessage.COMMAND_LOCATION_UPDATE_GO + e, e);
		}

		page = ConfigurationManager.getProperty("path.page.touragent.location.update");
		if (entity != null) {
			
			request.setAttribute("entity", entity);
			request.setAttribute("locationFindSuccessMessage", MessageManager
						.getProperty("message.location.find.success"));
			

			} else {
				request.setAttribute("locationFindErrorMessage",
						MessageManager
								.getProperty("message.location.find.error"));
				
			}

		return page;
	}

}



