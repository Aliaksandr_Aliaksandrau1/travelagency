package by.epam.travelagency.command.impl.location;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.command.exception.CommandException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.location.LocationCreateServiceImpl;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusCreateServiceImpl;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.LocaleManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class LocationCreateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
				
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();
		
		String country = request.getParameter(NamedConstant.PARAM_LOCATION_COUNTRY).trim();
		String city = request.getParameter(NamedConstant.PARAM_LOCATION_CITY).trim();
		String hotel = request.getParameter(NamedConstant.PARAM_LOCATION_HOTEL).trim();
		String description = request.getParameter(NamedConstant.PARAM_LOCATION_DESCRIPTION).trim();
	
		
		entityParameterMap.put("country", country);
		entityParameterMap.put("city", city);
		entityParameterMap.put("hotel", hotel);
		entityParameterMap.put("description", description);

		
		Location entity;
		try {
			entity = LocationCreateServiceImpl.getInstance().entityCreateNew(entityParameterMap);
		} catch (ServiceException e) {
			throw new CommandException( ExceptionMessage.COMMAND_LOCATION_CREATE + e, e);
		}
		
		
		
		
		String locale = request.getSession().getAttribute("local").toString();
		
		
		page = ConfigurationManager
				.getProperty("path.page.touragent.location.create");

	
		
		if (entity != null) {

			request.setAttribute("entity", entity);
			//request.setAttribute("locationCreateSuccessMessage", MessageManager.getProperty("message.location.create.success"));
			
			String locationCreateSuccessMessage = LocaleManager.getInstance()
					.getMessage("local.location.create.page.message.success.create",
							locale);

			request.setAttribute("locationCreateSuccessMessage", locationCreateSuccessMessage);
			

			} else {
				//request.setAttribute("locationCreateErrorMessage",	MessageManager	.getProperty("message.location.create.error"));
				
				//String locationCreateErrorMessage = LocaleManager.getInstance().getMessage("local.location.create.page.message.error.create",	locale);

			//	request.setAttribute("locationCreateErrorMessage", locationCreateErrorMessage);
				
				
				
			}

			return page;
	}

}



