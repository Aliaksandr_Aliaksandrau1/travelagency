package by.epam.travelagency.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.service.resource.ConfigurationManager;

public class EmptyCommand implements IActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = ConfigurationManager.getProperty("path.page.start_page");
		return page;
	}
}
