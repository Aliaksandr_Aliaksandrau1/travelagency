package by.epam.travelagency.command.impl.orderstatus;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.NamedConstant;


public class OrderStatusUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_ORDERSTATUS_ID).trim());
		String name = request.getParameter(NamedConstant.PARAM_ORDERSTATUS_NAME).trim();
		OrderStatus entity = new OrderStatus();
		entity.setId(id);
		entity.setName(name);

		request.setAttribute("entity", entity);
		page = ConfigurationManager.getProperty("path.page.admin.orderstatus.update");
		
		return page;
	}

}



