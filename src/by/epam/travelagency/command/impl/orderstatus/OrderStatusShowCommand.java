package by.epam.travelagency.command.impl.orderstatus;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class OrderStatusShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<OrderStatus> entityList = new ArrayList<OrderStatus>();
		String page = null;
		
		entityList = OrderStatusShowServiceImpl.getInstance().entityShow();

		page = ConfigurationManager
				.getProperty("path.page.admin.orderstatus.show");
		if (entityList != null) {
			request.setAttribute("orderStatusList", entityList);
			

		} else {
			request.setAttribute("orderStatusShowErrorMessage",
					MessageManager.getProperty("message.orderstatus.show.error"));
			
		}
		return page;
	}
}
