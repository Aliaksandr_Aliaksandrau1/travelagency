package by.epam.travelagency.command.impl.orderstatus;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusUpdateServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class OrderStatusUpdateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();

		String id = request.getParameter(NamedConstant.PARAM_ORDERSTATUS_ID)
				.trim();
		String name = request
				.getParameter(NamedConstant.PARAM_ORDERSTATUS_NAME).trim();

		entityParameterMap.put("name", name);
		entityParameterMap.put("id", id);

		OrderStatus entity = OrderStatusUpdateServiceImpl.getInstance()
				.entityUpdate(entityParameterMap);

		page = ConfigurationManager
				.getProperty("path.page.admin.orderstatus.update");

		if (entity != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("orderStatusUpdateSuccessMessage",
					MessageManager
							.getProperty("message.orderstatus.update.success"));

		}

		else {

			request.setAttribute("orderStatusUpdateErrorMessage",
					MessageManager
							.getProperty("message.orderstatus.update.error"));
		}
		return page;
	}

}