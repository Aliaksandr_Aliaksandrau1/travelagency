package by.epam.travelagency.command.impl.orderstatus;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusCreateServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;
import by.epam.travelagency.service.validation.impl.OrderStatusValidationImpl;

public class OrderStatusCreateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();

		String name = request
				.getParameter(NamedConstant.PARAM_ORDERSTATUS_NAME).trim();

		entityParameterMap.put("name", name);

		
		OrderStatus entity = OrderStatusCreateServiceImpl.getInstance()
				.entityCreateNew(entityParameterMap);
		
		page = ConfigurationManager
				.getProperty("path.page.admin.orderstatus.create");

				
		
		if (entity != null) {

		request.setAttribute("entity", entity);
		request.setAttribute("orderStatusCreateSuccessMessage", MessageManager
					.getProperty("message.orderstatus.create.success"));

			

		} else {
			request.setAttribute("orderStatusCreateErrorMessage",
					MessageManager
							.getProperty("message.orderstatus.create.error"));
			
		}

		return page;
	}

}
