package by.epam.travelagency.command.impl.orderstatus;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class OrderStatusDeleteCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		List<OrderStatus> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request.getParameter(
				NamedConstant.PARAM_ORDERSTATUS_ID).trim());

		isDeleted = OrderStatusDeleteServiceImpl.getInstance().entityDeleteNew(
				id);

		entityList = OrderStatusShowServiceImpl.getInstance().entityShow();

		request.setAttribute("orderStatusList", entityList);

		page = ConfigurationManager
				.getProperty("path.page.admin.orderstatus.show");

		if (isDeleted) {

			request.setAttribute("orderStatusDeleteSuccessMessage",
					MessageManager
							.getProperty("message.orderstatus.delete.success"));

		}

		else {

			request.setAttribute("orderStatusDeleteErrorMessage",
					MessageManager
							.getProperty("message.orderstatus.delete.error"));

		}

		return page;
	}

}
