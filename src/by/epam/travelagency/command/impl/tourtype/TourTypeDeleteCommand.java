package by.epam.travelagency.command.impl.tourtype;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class TourTypeDeleteCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		List<TourType> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request
				.getParameter(NamedConstant.PARAM_TOURTYPE_ID).trim());

		

		isDeleted = TourTypeDeleteServiceImpl.getInstance().entityDeleteNew(
				id);

		entityList = TourTypeShowServiceImpl.getInstance().entityShow();

		request.setAttribute("tourTypeList", entityList);
	
		page = ConfigurationManager
				.getProperty("path.page.admin.tourtype.show"); 
		if (isDeleted) {

			request.setAttribute("tourTypeDeleteSuccessMessage",
					MessageManager
							.getProperty("message.tourtype.delete.success"));
			
		}

		else {
			
			request.setAttribute("tourTypeDeleteErrorMessage",
					MessageManager
							.getProperty("message.tourtype.delete.error"));
			
		}
		return page;
	}

}
