package by.epam.travelagency.command.impl.tourtype;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.NamedConstant;


public class TourTypeUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_TOURTYPE_ID).trim());
		String name = request
				.getParameter(NamedConstant.PARAM_TOURTYPE_NAME).trim();
		
	
		
		TourType entity = new TourType();
		entity.setId(id);
		entity.setName(name);
		request.setAttribute("entity", entity);
	
		
		page = ConfigurationManager.getProperty("path.page.admin.tourtype.update");
		
		
		return page;
	}

}



