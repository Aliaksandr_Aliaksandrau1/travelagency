package by.epam.travelagency.command.impl.tourtype;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;








import org.apache.log4j.Logger;




import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeCreateServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class TourTypeCreateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map <String, String> entityParameterMap = new HashMap <String,String>();
	
		String name = request.getParameter(NamedConstant.PARAM_TOURTYPE_NAME).trim();
		entityParameterMap.put("name", name);
				
		TourType entity = TourTypeCreateServiceImpl.getInstance()
				.entityCreateNew(entityParameterMap);
		
		page = ConfigurationManager.getProperty("path.page.admin.tourtype.create");
			
		
		if (entity != null) {
				
		request.setAttribute("entity", entity);
		request.setAttribute("tourTypeCreateSuccessMessage",
					MessageManager.getProperty("message.tourtype.create.success"));
			
		
		} else {
		request.setAttribute("tourTypeCreateErrorMessage",
							MessageManager.getProperty("message.tourtype.create.error")); 
		
		}
	
		
		return page;
	}

}



