package by.epam.travelagency.command.impl.tourtype;

import javax.servlet.http.HttpServletRequest;




import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;


import by.epam.travelagency.service.resource.ConfigurationManager;


public class TourTypeCreateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
			
		String page = ConfigurationManager.getProperty("path.page.admin.tourtype.create");
			
		return page;
	}

}



