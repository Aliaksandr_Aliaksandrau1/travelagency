package by.epam.travelagency.command.impl.tourtype;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class TourTypeShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<TourType> entityList = new ArrayList<TourType>();
		String page = null;

		entityList = TourTypeShowServiceImpl.getInstance().entityShow();

		page = ConfigurationManager
				.getProperty("path.page.admin.tourtype.show");
		
		if (entityList != null) {

			request.setAttribute("tourTypeList", entityList);
			

		} else {
			request.setAttribute("tourTypeShowErrorMessage",
					MessageManager.getProperty("message.tourtype.show.error"));
		}
		return page;
	}
}
