package by.epam.travelagency.command.impl.tour;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;











import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusUpdateServiceImpl;
import by.epam.travelagency.service.entityservice.tour.TourUpdateService;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class TourUpdateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		
		
		
		
		String page = null;
		Map <String, String> entityParameterMap = new HashMap <String,String>();
		
		String id = request.getParameter(NamedConstant.PARAM_TOUR_ID);
		String title = request.getParameter(NamedConstant.PARAM_TOUR_TITLE);
		String beginDate = request.getParameter(NamedConstant.PARAM_TOUR_BEGIN_DATE);
		String endDate = request.getParameter(NamedConstant.PARAM_TOUR_END_DATE);
		String tourTypeId = request.getParameter(NamedConstant.PARAM_TOUR_TOURTYPE);
		String tourStatusId = request.getParameter(NamedConstant.PARAM_TOUR_TOURSTATUS);
		String price = request.getParameter(NamedConstant.PARAM_TOUR_PRICE);
		String description = request.getParameter(NamedConstant.PARAM_TOUR_DESCRIPTION);
		
		String location = request.getParameter(NamedConstant.PARAM_TOUR_LOCATION);
		
		System.out.println("!!!!!!!!!!!!" + title + description);
			
		
		entityParameterMap.put("id", id);
		entityParameterMap.put("title", title);
		entityParameterMap.put("beginDate", beginDate);
		entityParameterMap.put("endDate", endDate);
		entityParameterMap.put("tourTypeId", tourTypeId);
		entityParameterMap.put("tourStatusId", tourStatusId);
		entityParameterMap.put("price", price);
		entityParameterMap.put("description", description);
		entityParameterMap.put("location", location);
		
		
		//List<OrderStatus> entityList = null;
		//entityList = OrderStatusUpdateService.entityUpdate(id, name);
		
		Tour updatedEntity = TourUpdateService.entityUpdate(entityParameterMap);
		
	
		
		
		if (updatedEntity != null) {

			request.setAttribute("entity", updatedEntity);
			//request.setAttribute("locationdeletesuccess",MessageManager.getProperty("message.locationDeleteSuccess"));
			page = ConfigurationManager
					.getProperty("path.page.tour.update");

		}
		
	
	else {
			// request.setAttribute("errorLocationAddMessage",
			// MessageManager.getProperty("message.locationadderror"));
			page = ConfigurationManager
					.getProperty("path.page.tour.update");
		}

		return page;
	}

}
