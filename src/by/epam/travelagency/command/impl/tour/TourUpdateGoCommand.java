package by.epam.travelagency.command.impl.tour;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import by.epam.travelagency.service.entityservice.tour.TourUpdateGoService;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusShowServiceImpl;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeShowServiceImpl;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;


public class TourUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_TOUR_ID));

		Tour entity = TourUpdateGoService.getInstance().entityGetById(id);
		List<Location> locationList = null;
		List<TourType> tourTypeList = null;
		List<TourStatus> tourStatusList = null;
		
		
		try {
			locationList = LocationShowServiceImpl.getInstance().entityShow();
			tourTypeList = TourTypeShowServiceImpl.getInstance().entityShow();
			tourStatusList = TourStatusShowServiceImpl.getInstance().entityShow();
			
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block    // доработать
			e.printStackTrace();
		}
		
	
		
		//System.out.println("!!!!!!!!!!!!!!!!!!id " + id);
		//System.out.println("!!!!!!!!!!!!!!!!!!entity " + entity);
		
	
		
			
		if (locationList != null && tourTypeList!=null && tourStatusList!=null) {

			
			request.setAttribute("entity", entity);
			request.setAttribute("locationList", locationList);
			request.setAttribute("tourTypeList", tourTypeList);
			request.setAttribute("tourStatusList", tourStatusList);
				
			page = ConfigurationManager.getProperty("path.page.tour.update");
	
		
		
		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.tour.update");
		}
		
		
		
		
		return page;
	}

}



