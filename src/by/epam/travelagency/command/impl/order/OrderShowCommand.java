package by.epam.travelagency.command.impl.order;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;






import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.entityservice.order.OrderShowService;
import by.epam.travelagency.service.entityservice.tour.TourShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class OrderShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<Order> entityList = new ArrayList<Order>();
		
		
	
		
		
		String page = null;

		
			entityList = OrderShowService.getInstance().entityShow();
		

		
		if (entityList != null) {

			request.setAttribute("orderList", entityList);
			
			page = ConfigurationManager
					.getProperty("path.page.order.show");

		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.order.show"); // доработать
		}
		return page;
	}
}
