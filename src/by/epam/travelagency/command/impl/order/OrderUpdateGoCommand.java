package by.epam.travelagency.command.impl.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.order.OrderUpdateGoService;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;


public class OrderUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		Order entity = null;
		List<OrderStatus> orderStatusList = null;
		
		
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_ORDER_ID));
				System.out.println(id);
	
		
		entity = OrderUpdateGoService.getInstance().entityUpdate(id);
		orderStatusList = OrderStatusShowServiceImpl.getInstance().entityShow();
		
		System.out.println(entity);
		
		if (entity != null && orderStatusList != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("orderStatusList", orderStatusList);
			
			page = ConfigurationManager.getProperty("path.page.order.update");

		} else {
			request.setAttribute("errorLoginPassMessage",
					MessageManager.getProperty("message.loginerror"));
			page = ConfigurationManager.getProperty("path.page.order.update"); // доработать
		}
		
		return page;
	}

}




