package by.epam.travelagency.command.impl.order;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.service.entityservice.order.OrderCreateService;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class OrderCreateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();

		entityParameterMap.put("userId",
				request.getParameter(NamedConstant.PARAM_ORDER_USER_ID));
		entityParameterMap.put("tourId",
				request.getParameter(NamedConstant.PARAM_ORDER_TOUR_ID));
		entityParameterMap
				.put("orderStatusId",
						request.getParameter(NamedConstant.PARAM_ORDER_ORDER_STATUS_ID));

		logger.debug("OrderCreateCommand" + entityParameterMap); // !!!

		OrderCreateService entityCreateService = new OrderCreateService();

		if (entityCreateService.entityCreate(entityParameterMap)) {

			request.setAttribute("locationaddsuccess",
					MessageManager.getProperty("message.locationaddsuccess"));
			page = ConfigurationManager.getProperty("path.page.tour.show");

		} else {
			request.setAttribute("errorLocationAddMessage",
					MessageManager.getProperty("message.locationadderror"));
			page = ConfigurationManager.getProperty("path.page.tour.show");
		}

		return page;
	}

}
