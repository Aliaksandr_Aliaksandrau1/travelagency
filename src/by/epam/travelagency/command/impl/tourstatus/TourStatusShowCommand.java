package by.epam.travelagency.command.impl.tourstatus;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusShowServiceImpl;
import by.epam.travelagency.service.entityservice.tourtype.TourTypeShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class TourStatusShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<TourStatus> entityList = new ArrayList<TourStatus>();
		String page = null;
		
		entityList = TourStatusShowServiceImpl.getInstance().entityShow();


		page = ConfigurationManager
				.getProperty("path.page.admin.tourstatus.show");
		
		if (entityList != null) {

			request.setAttribute("tourStatusList", entityList);
			

		} else {
			request.setAttribute("tourStatusShowErrorMessage",
					MessageManager.getProperty("message.tourstatus.show.error"));
		
		}
		return page;
	}
}
