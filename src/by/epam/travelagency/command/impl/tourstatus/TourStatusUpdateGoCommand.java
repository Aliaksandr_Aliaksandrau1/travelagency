package by.epam.travelagency.command.impl.tourstatus;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.NamedConstant;


public class TourStatusUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
				
		String page = null;
		int id = Integer.parseInt(request.getParameter(NamedConstant.PARAM_TOURSTATUS_ID).trim());
		String name = request
				.getParameter(NamedConstant.PARAM_TOURSTATUS_NAME).trim();
		
		TourStatus entity = new TourStatus();
		entity.setId(id);
		entity.setName(name);
		request.setAttribute("entity", entity);
	
		
		page = ConfigurationManager.getProperty("path.page.admin.tourstatus.update");
		
		
		return page;
	}

}



