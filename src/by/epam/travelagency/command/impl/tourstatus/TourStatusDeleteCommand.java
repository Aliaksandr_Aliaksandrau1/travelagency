package by.epam.travelagency.command.impl.tourstatus;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;








import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.tourstatus.TourStatusShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class TourStatusDeleteCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		List<TourStatus> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request
				.getParameter(NamedConstant.PARAM_TOURSTATUS_ID).trim());

		

		isDeleted = TourStatusDeleteServiceImpl.getInstance().entityDeleteNew(
				id);

		entityList = TourStatusShowServiceImpl.getInstance().entityShow();

		request.setAttribute("tourStatusList", entityList);
		page = ConfigurationManager
				.getProperty("path.page.admin.tourstatus.show"); 
		
		if (isDeleted) {

			request.setAttribute("tourStatusDeleteSuccessMessage",
					MessageManager
							.getProperty("message.tourstatus.delete.success"));

		
		}

		else {
			
			request.setAttribute("tourStatusDeleteErrorMessage",
					MessageManager
							.getProperty("message.tourstatus.delete.error"));
			
		}

		return page;
	}

}
