package by.epam.travelagency.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserLogoutCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	@Override
	public String execute(HttpServletRequest request) {
	
		String page = ConfigurationManager.getProperty("path.page.start_page");
		request.getSession().invalidate();
		request.getSession().setAttribute(NamedConstant.PARAM_USER_ROLE,  NamedConstant.PARAM_USER_ROLE_BY_DEFAULT);
	
		return page;
	}

}
