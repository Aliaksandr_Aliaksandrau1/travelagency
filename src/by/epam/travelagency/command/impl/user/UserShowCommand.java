package by.epam.travelagency.command.impl.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.user.UserShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;

public class UserShowCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		List<User> entityList = new ArrayList<User>();
		String page = null;

		entityList = UserShowServiceImpl.getInstance().entityShow();
		page = ConfigurationManager
				.getProperty("path.page.admin.user.show");

		if (entityList != null) {

			request.setAttribute("entityList", entityList);

		} else {
			request.setAttribute("locationShowErrorMessage",
					MessageManager.getProperty("message.location.show.error"));

		}
		return page;
	}
}
