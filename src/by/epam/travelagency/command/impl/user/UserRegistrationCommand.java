package by.epam.travelagency.command.impl.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.user.UserCreateServiceImpl;
import by.epam.travelagency.service.entityservice.user.UserRegistrationService;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.LocaleManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserRegistrationCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();
	
	@Override
	public String execute(HttpServletRequest request) {
		
				
		String page = null;
		Map<String, String> entityParameterMap = new HashMap<String, String>();
		
		String login = request.getParameter(NamedConstant.PARAM_USER_LOGIN).trim();
		String password = request.getParameter(NamedConstant.PARAM_USER_PASSWORD).trim();
		
		String name = request.getParameter(NamedConstant.PARAM_USER_NAME).trim();
		String surname = request.getParameter(NamedConstant.PARAM_USER_SURNAME).trim();
		String telnumber = request.getParameter(NamedConstant.PARAM_USER_TELNUMBER).trim();
		String email = request.getParameter(NamedConstant.PARAM_USER_EMAIL).trim();

		String role = request.getParameter(NamedConstant.PARAM_USER_ROLE).trim();
		//String role = "customer";
		String discount = request.getParameter(NamedConstant.PARAM_USER_DISCOUNT).trim();
		//String discount = "0";
		
		entityParameterMap.put("login", login);
		entityParameterMap.put("password", password);
		entityParameterMap.put("role", role);
		entityParameterMap.put("name", name);
		entityParameterMap.put("surname", surname);
		entityParameterMap.put("telnumber", telnumber);
		entityParameterMap.put("email", email);
		entityParameterMap.put("discount", discount);
		
		User entity = UserCreateServiceImpl.getInstance().entityCreateNew(entityParameterMap);
		
		HttpSession session = request.getSession(true);
		String locale = session.getAttribute("local").toString();
		
		
				
				if (entity != null) {
					
				
					session.setAttribute("user", entity);
				
					page = ConfigurationManager.getProperty("path.page.main");
					
				} else {
										
					String errorRegistrationMessage = LocaleManager.getInstance()
							.getMessage("local.user.registration.page.message.error.registration", locale);

					request.setAttribute("errorRegistrationMessage", errorRegistrationMessage);
					
					
					page = ConfigurationManager.getProperty("path.page.user.registration");  
				}
		

		
		return page;
	}

}



