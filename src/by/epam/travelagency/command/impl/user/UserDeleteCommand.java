package by.epam.travelagency.command.impl.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.location.LocationDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationShowServiceImpl;
import by.epam.travelagency.service.entityservice.user.UserDeleteServiceImpl;
import by.epam.travelagency.service.entityservice.user.UserShowServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserDeleteCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		List<User> entityList = null;
		boolean isDeleted = false;

		int id = Integer.parseInt(request.getParameter(
				NamedConstant.PARAM_USER_ID).trim());

		isDeleted = UserDeleteServiceImpl.getInstance().entityDeleteNew(id);

		entityList = UserShowServiceImpl.getInstance().entityShow();

		request.setAttribute("entityList", entityList);
		page = ConfigurationManager
				.getProperty("path.page.admin.user.show");

		if (isDeleted) {

			request.setAttribute("locationDeleteSuccessMessage", MessageManager
					.getProperty("message.location.delete.success"));

		}

		else {

			request.setAttribute("orderStatusDeleteErrorMessage",
					MessageManager.getProperty("message.location.delete.error"));
		}

		return page;
	}

}
