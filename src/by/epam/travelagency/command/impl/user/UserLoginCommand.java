package by.epam.travelagency.command.impl.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.user.UserLoginService;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.LocaleManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserLoginCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {
		User user = null;
		String page = null;
		String login = request.getParameter(NamedConstant.PARAM_USER_LOGIN)
				.trim();
		String password = request.getParameter(
				NamedConstant.PARAM_USER_PASSWORD).trim();
		user = UserLoginService.getInstance().checkLogin(login, password);

		HttpSession session = request.getSession();
		String locale = session.getAttribute("local").toString();

		if (user != null) {

			session.setAttribute("user", user);
			page = ConfigurationManager.getProperty("path.page.main");

	
			
			
		} else {

			String errorLoginPassMessage = LocaleManager.getInstance()
					.getMessage("local.user.login.page.message.error.login",
							locale);

			request.setAttribute("errorLoginPassMessage", errorLoginPassMessage);
			page = ConfigurationManager.getProperty("path.page.user.login");
		}
		return page;
	}
}
