package by.epam.travelagency.command.impl.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.user.UserUpdateServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserUpdateCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		Map<String, String> entityParameterMap = new HashMap<String, String>();

		
		
		
		String id = request.getParameter(NamedConstant.PARAM_USER_ID)
				.trim();
				
		String login = request.getParameter(NamedConstant.PARAM_USER_LOGIN).trim();
		String password = request.getParameter(NamedConstant.PARAM_USER_PASSWORD).trim();
		String role = request.getParameter(NamedConstant.PARAM_USER_ROLE).trim();
		String name = request.getParameter(NamedConstant.PARAM_USER_NAME).trim();
		String surname = request.getParameter(NamedConstant.PARAM_USER_SURNAME).trim();
		String telnumber = request.getParameter(NamedConstant.PARAM_USER_TELNUMBER).trim();
		String email = request.getParameter(NamedConstant.PARAM_USER_EMAIL).trim();
		String discount = request.getParameter(NamedConstant.PARAM_USER_DISCOUNT).trim();

		entityParameterMap.put("id", id);
		entityParameterMap.put("login", login);
		entityParameterMap.put("password", password);
		entityParameterMap.put("role", role);
		entityParameterMap.put("name", name);
		entityParameterMap.put("surname", surname);
		entityParameterMap.put("telnumber", telnumber);
		entityParameterMap.put("email", email);
		entityParameterMap.put("discount", discount);
		
		
		
		
		User entity = UserUpdateServiceImpl.getInstance().entityUpdate(
				entityParameterMap);
		page = ConfigurationManager
				.getProperty("path.page.admin.user.update");

		if (entity != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("locationUpdateSuccessMessage", MessageManager
					.getProperty("message.location.update.success"));

		} else {

			request.setAttribute("locationUpdateErrorMessage",
					MessageManager.getProperty("message.location.update.error"));

		}
		return page;
	}

}
