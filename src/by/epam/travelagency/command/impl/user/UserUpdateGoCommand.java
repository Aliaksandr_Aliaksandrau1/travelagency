package by.epam.travelagency.command.impl.user;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.travelagency.command.IActionCommand;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.location.LocationCreateServiceImpl;
import by.epam.travelagency.service.entityservice.location.LocationUpdateGoServiceImpl;
import by.epam.travelagency.service.entityservice.user.UserUpdateGoServiceImpl;
import by.epam.travelagency.service.resource.ConfigurationManager;
import by.epam.travelagency.service.resource.MessageManager;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserUpdateGoCommand implements IActionCommand {
	private final static Logger logger = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int id = Integer.parseInt(request.getParameter(
				NamedConstant.PARAM_USER_ID).trim());

		User entity = UserUpdateGoServiceImpl.getInstance().entityToUpdate(id);

		page = ConfigurationManager.getProperty("path.page.admin.user.update");
		if (entity != null) {

			request.setAttribute("entity", entity);
			request.setAttribute("locationFindSuccessMessage",
					MessageManager.getProperty("message.location.find.success"));

		} else {
			request.setAttribute("locationFindErrorMessage",
					MessageManager.getProperty("message.location.find.error"));

		}

		return page;
	}

}
