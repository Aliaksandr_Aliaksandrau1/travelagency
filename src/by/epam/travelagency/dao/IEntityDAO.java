package by.epam.travelagency.dao;

import java.util.List;

import by.epam.travelagency.entity.Entity;

public interface IEntityDAO<T extends Entity> {

	public List<T> findAll() throws DAOException;

	public boolean create(T entity) throws DAOException;

	public T findEntityById(int id) throws DAOException;

	public boolean delete(int id) throws DAOException;
	
	public boolean update(T entity) throws DAOException;

}
