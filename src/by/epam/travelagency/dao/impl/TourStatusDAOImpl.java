package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.IEntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.resource.NamedConstant;


public class TourStatusDAOImpl implements IEntityDAO<TourStatus> {
	private final static Logger logger = Logger.getRootLogger();

	private TourStatusDAOImpl() {

	}

	private final static TourStatusDAOImpl instance = new TourStatusDAOImpl();

	public static TourStatusDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<TourStatus> findAll() {
		List<TourStatus> entityList = null;
		TourStatus entity = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_FIND_ALL);
			ResultSet resultSet = statement.executeQuery();

			entityList = new ArrayList<TourStatus>();

			while (resultSet.next()) {
				entity = new TourStatus();

				entity.setId(resultSet.getInt(NamedConstant.DB_TOURSTATUS_ID));
				entity.setName(resultSet
						.getString(NamedConstant.DB_TOURSTATUS_NAME));
				entityList.add(entity);
					

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;

	}

	@Override
	public boolean create(TourStatus entity) {
	
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_CREATE);

			statement.setString(1, entity.getName());
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isCreated = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public TourStatus findEntityById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(TourStatus entity) {
		boolean isUpdated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_UPDATE);

			statement.setString(1, entity.getName());
			statement.setInt(2, entity.getId());
						
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
