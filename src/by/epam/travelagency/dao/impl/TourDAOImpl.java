package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.IEntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.resource.NamedConstant;



public class TourDAOImpl implements IEntityDAO<Tour> {
	private final static Logger logger = Logger.getRootLogger();
	private TourDAOImpl() {
	}
	private final static TourDAOImpl instance = new TourDAOImpl();
	public static TourDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<Tour> findAll() {
		List<Tour> entityList = null;
		Tour entity = null;
		TourType tourType = null;
		TourStatus tourStatus = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.TOUR_FIND_ALL);
			resultSet = statement.executeQuery();

			entityList = new ArrayList<Tour>();

			while (resultSet.next()) {
				entity = new Tour();
				tourType = new TourType(); 
				tourStatus = new TourStatus(); 
				
				tourType.setId(resultSet.getInt(NamedConstant.DB_TOURTYPE_ID));
				tourType.setName(resultSet.getString(NamedConstant.DB_TOURTYPE_NAME));
				
				tourStatus.setId(resultSet.getInt(NamedConstant.DB_TOURSTATUS_ID));
				tourStatus.setName(resultSet.getString(NamedConstant.DB_TOURSTATUS_NAME));
				
				
				entity.setId(resultSet.getInt(NamedConstant.DB_TOUR_ID));
				entity.setTitle(resultSet
						.getString(NamedConstant.DB_TOUR_TITLE));
				entity.setBeginDate(resultSet
						.getDate(NamedConstant.DB_TOUR_BEGIN_DATE));
				entity.setEndDate(resultSet
						.getDate(NamedConstant.DB_TOUR_END_DATE));
				
				entity.setPrice(resultSet
						.getDouble(NamedConstant.DB_TOUR_PRICE));
				
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_TOUR_DESCRIPTION));
				entityList.add(entity);
				
				
				entity.setTourType(tourType);	
				entity.setTourStatus(tourStatus);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;

	}

	@Override
	public boolean create(Tour entity) {
	
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOUR_CREATE);

			
			statement.setString(1, entity.getTitle());
			statement.setDate(2, sqlDateFromDate(entity.getBeginDate()));   // ���������� �����������!!!!!!!
			statement.setDate(3, sqlDateFromDate(entity.getEndDate()));		// ���������� �����������!!!!!!!
			statement.setInt(4, entity.getTourType().getId());
			statement.setDouble(5, entity.getPrice());
			statement.setInt(6, entity.getTourStatus().getId());
			statement.setString(7, entity.getDescription());
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// ���������� � ���������
	
	java.sql.Date sqlDateFromDate (java.util.Date date) {
		
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		java.sql.Date sqlDate = java.sql.Date.valueOf(localDate);
		
		
		return sqlDate;
	}
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	@Override
	public Tour findEntityById(int id) {
		Tour entity = null;
		TourType tourType = null;
		TourStatus tourStatus = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.TOUR_FIND_BY_ID);
			
			
			statement.setInt(1, id);
			
			resultSet = statement.executeQuery();

			System.out.println("DAO!!!!! " + id);

			while (resultSet.next()) {
				entity = new Tour();
				tourType = new TourType(); 
				tourStatus = new TourStatus(); 
				
				tourType.setId(resultSet.getInt(NamedConstant.DB_TOURTYPE_ID));
				tourType.setName(resultSet.getString(NamedConstant.DB_TOURTYPE_NAME));
				
				tourStatus.setId(resultSet.getInt(NamedConstant.DB_TOURSTATUS_ID));
				tourStatus.setName(resultSet.getString(NamedConstant.DB_TOURSTATUS_NAME));
				
				
				entity.setId(resultSet.getInt(NamedConstant.DB_TOUR_ID));
				entity.setTitle(resultSet
						.getString(NamedConstant.DB_TOUR_TITLE));
				entity.setBeginDate(resultSet
						.getDate(NamedConstant.DB_TOUR_BEGIN_DATE));
				entity.setEndDate(resultSet
						.getDate(NamedConstant.DB_TOUR_END_DATE));
				
				entity.setPrice(resultSet
						.getDouble(NamedConstant.DB_TOUR_PRICE));
				
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_TOUR_DESCRIPTION));
				
				entity.setTourType(tourType);	
				entity.setTourStatus(tourStatus);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entity;
	}

	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOUR_DELETE);

			statement.setInt(1, id);
					
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(Tour entity) {
		boolean isUpdated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOUR_UPDATE);

			statement.setString(1, entity.getTitle());
			statement.setDate(2, sqlDateFromDate(entity.getBeginDate()));   // ���������� �����������!!!!!!!
			statement.setDate(3, sqlDateFromDate(entity.getEndDate()));		// ���������� �����������!!!!!!!
			statement.setInt(4, entity.getTourType().getId());
			statement.setDouble(5, entity.getPrice());
			statement.setInt(6, entity.getTourStatus().getId());
			statement.setString(7, entity.getDescription());
			statement.setInt(8, entity.getId());			
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
