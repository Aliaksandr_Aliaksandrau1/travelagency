package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.IEntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.Location2Tour;
import by.epam.travelagency.service.resource.NamedConstant;

public class Location2TourDAOImpl implements IEntityDAO<Location2Tour> {
	private final static Logger logger = Logger.getRootLogger();

	private Location2TourDAOImpl() {

	}

	private final static Location2TourDAOImpl instance = new Location2TourDAOImpl();

	public static Location2TourDAOImpl getInstance() {
		return instance;
	}

	// ������� ��������� � ������� getLoc2TourListByTourId() ��� Location2TourDAOImpl
	public List<Location2Tour> getLoc2TourListByTourId(int id) {
		List<Location2Tour> entityList = null;
		Location2Tour entity = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION2TOUR_FIND_LOCATION_BY_TOUR_ID);
			
			statement.setInt(1, id);
			
			
			
			ResultSet resultSet = statement.executeQuery();

			entityList = new ArrayList<Location2Tour>();

			

			while (resultSet.next()) {
				entity = new Location2Tour();

				entity.setId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_ID));
				entity.setLocationId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_LOCATION_ID));
				entity.setTourId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_TOUR_ID));
				entityList.add(entity);

			}

			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;
	}

	@Override
	public List<Location2Tour> findAll() {
		List<Location2Tour> entityList = null;
		Location2Tour entity = null;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			PreparedStatement statement = connection
					.prepareStatement(Query.LOCATION2TOUR_FIND_ALL);
			ResultSet resultSet = statement.executeQuery();

			entityList = new ArrayList<Location2Tour>();

			while (resultSet.next()) {
				entity = new Location2Tour();

				entity.setId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_ID));
				entity.setLocationId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_LOCATION_ID));
				entity.setTourId(resultSet
						.getInt(NamedConstant.DB_LOCATION2TOUR_TOUR_ID));
				entityList.add(entity);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entityList;

	}

	@Override
	public boolean create(Location2Tour entity) {

		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_CREATE);

			// statement.setString(1, entity.getName());

			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public Location2Tour findEntityById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	}

	@Override
	public boolean update(Location2Tour entity) {
		boolean isUpdated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.TOURSTATUS_UPDATE);

			// statement.setString(1, entity.getName());
			statement.setInt(2, entity.getId());

			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}

}
