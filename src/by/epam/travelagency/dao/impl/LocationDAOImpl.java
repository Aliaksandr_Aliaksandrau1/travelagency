package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.IEntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.resource.NamedConstant;

public class LocationDAOImpl implements IEntityDAO<Location> {
	private final static Logger logger = Logger.getRootLogger();

	private LocationDAOImpl() {
	}

	private final static LocationDAOImpl instance = new LocationDAOImpl();

	public static LocationDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<Location> findAll() throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<Location> locationList = null;
		Location entity = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(Query.LOCATION_FIND_ALL);
			resultSet = statement.executeQuery();
			locationList = new ArrayList<Location>();

			while (resultSet.next()) {
				entity = new Location();
				entity.setId(resultSet.getInt(NamedConstant.DB_LOCATION_ID));
				entity.setCountry(resultSet
						.getString(NamedConstant.DB_LOCATION_COUNTRY));
				entity.setCity(resultSet
						.getString(NamedConstant.DB_LOCATION_CITY));
				entity.setHotel(resultSet
						.getString(NamedConstant.DB_LOCATION_HOTEL));
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_LOCATION_DESCRIPTION));
				locationList.add(entity);
			}
		} catch (SQLException e) {

			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} catch (ConnectionPoolException e) {

			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} finally {

			if (connection != null) {
				ConnectionPool.getInstance().closeConnection(connection);
			}

			try {

				if (statement != null) {
					statement.close();
				}

			} catch (SQLException e) {

				throw new DAOException(
						ExceptionMessage.DAO_DATABASE_CLOSE_CONNECTION + e, e);
			}
		}
		return locationList;
	}

	@Override
	public boolean create(Location location) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		boolean isCreated = false;
		try {

			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(Query.LOCATION_CREATE);

			statement.setString(1, location.getCountry());
			statement.setString(2, location.getCity());
			statement.setString(3, location.getHotel());
			statement.setString(4, location.getDescription());
			statement.executeUpdate();
			isCreated = true;
		} catch (SQLException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} catch (ConnectionPoolException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} finally {
			if (connection != null) {
				ConnectionPool.getInstance().closeConnection(connection);
			}

			try {

				if (statement != null) {
					statement.close();
				}

			} catch (SQLException e) {

				throw new DAOException(
						ExceptionMessage.DAO_DATABASE_CLOSE_CONNECTION + e, e);
			}
		}
		return isCreated;
	}

	@Override
	public Location findEntityById(int id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Location entity = null;
		try {

			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(Query.LOCATION_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				entity = new Location();
				entity.setId(resultSet.getInt(NamedConstant.DB_LOCATION_ID));
				entity.setCountry(resultSet
						.getString(NamedConstant.DB_LOCATION_COUNTRY));
				entity.setCity(resultSet
						.getString(NamedConstant.DB_LOCATION_CITY));
				entity.setHotel(resultSet
						.getString(NamedConstant.DB_LOCATION_HOTEL));
				entity.setDescription(resultSet
						.getString(NamedConstant.DB_LOCATION_DESCRIPTION));

			}

		} catch (SQLException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} catch (ConnectionPoolException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);
		} finally {
			if (connection != null) {
				ConnectionPool.getInstance().closeConnection(connection);
			}

			try {

				if (statement != null) {
					statement.close();
				}

			} catch (SQLException e) {

				throw new DAOException(
						ExceptionMessage.DAO_DATABASE_CLOSE_CONNECTION + e, e);
			}
		}

		return entity;
	}

	@Override
	public boolean delete(int id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		boolean isDeleted = false;

		try {

			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(Query.LOCATION_DELETE);
			statement.setInt(1, id);
			statement.executeUpdate();
			isDeleted = true;

		} catch (SQLException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} catch (ConnectionPoolException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);
		} finally {
			if (connection != null) {
				ConnectionPool.getInstance().closeConnection(connection);
			}

			try {

				if (statement != null) {
					statement.close();
				}

			} catch (SQLException e) {

				throw new DAOException(
						ExceptionMessage.DAO_DATABASE_CLOSE_CONNECTION + e, e);
			}
		}

		return isDeleted;
	}

	@Override
	public boolean update(Location location) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		boolean isUpdated = false;
		try {

			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(Query.LOCATION_UPDATE);
			statement.setString(1, location.getCountry());
			statement.setString(2, location.getCity());
			statement.setString(3, location.getHotel());
			statement.setString(4, location.getDescription());
			statement.setInt(5, location.getId());
			statement.executeUpdate();
			isUpdated = true;
		} catch (SQLException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);

		} catch (ConnectionPoolException e) {
			throw new DAOException(
					ExceptionMessage.DAO_DATABASE_CONNECTION + e, e);
		} finally {
			if (connection != null) {
				ConnectionPool.getInstance().closeConnection(connection);
			}

			try {

				if (statement != null) {
					statement.close();
				}

			} catch (SQLException e) {

				throw new DAOException(
						ExceptionMessage.DAO_DATABASE_CLOSE_CONNECTION + e, e);
			}
		}
		return isUpdated;
	}

}
