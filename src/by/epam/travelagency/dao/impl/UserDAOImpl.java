package by.epam.travelagency.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.IEntityDAO;
import by.epam.travelagency.dao.Query;
import by.epam.travelagency.dao.IUserDAO;
import by.epam.travelagency.dao.connectionpool.ConnectionPool;
import by.epam.travelagency.dao.connectionpool.ConnectionPoolException;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.resource.NamedConstant;

public class UserDAOImpl implements IUserDAO, IEntityDAO<User> {
	private final static Logger logger = Logger.getRootLogger();

	private UserDAOImpl() {
	}

	private final static UserDAOImpl instance = new UserDAOImpl();

	public static UserDAOImpl getInstance() {
		return instance;
	}

	@Override
	public List<User> findAll() {
		List<User> userList = null;
		User entity = null;
		try {
			
			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			ResultSet resultSet = null;

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_ALL);

			resultSet = statement.executeQuery();
			
			userList = new ArrayList<User>();
			while (resultSet.next()) {
				
				entity = new User();
				
				entity.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
				entity.setLogin(resultSet.getString(NamedConstant.DB_USER_LOGIN));
				entity.setPassword(resultSet.getString(NamedConstant.DB_USER_PASSWORD));
				entity.setRole(resultSet.getString(NamedConstant.DB_USER_ROLE));
				entity.setName(resultSet.getString(NamedConstant.DB_USER_NAME));
				entity.setSurname(resultSet.getString(NamedConstant.DB_USER_SURNAME));
				entity.setTelephoneNumber(resultSet.getString(NamedConstant.DB_USER_TELNUMBER));
				entity.setEmail(resultSet.getString(NamedConstant.DB_USER_EMAIL));
				entity.setDiscount(resultSet.getDouble(NamedConstant.DB_USER_DISCOUNT));
				userList.add(entity);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return userList;
	}

	@Override
	public boolean create(User user) {
		boolean isCreated = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_CREATE);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getRole());
			statement.setString(4, user.getName());
			statement.setString(5, user.getSurname());
			statement.setString(6, user.getTelephoneNumber());
			statement.setString(7, user.getEmail());
			statement.setDouble(8, user.getDiscount());
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isCreated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isCreated;
	}

	@Override
	public User findEntityById(int id) {
		User user = null;
		try {
			
			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_BY_ID);

			statement.setInt(1, id);

			ResultSet resultSet = statement.executeQuery();

			if (!resultSet.wasNull()) { // обязательно переделать
										// проверку!!!!!!!!!!!!!!

				while (resultSet.next()) {

					user = new User();
					user.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
					user.setLogin(resultSet
							.getString(NamedConstant.DB_USER_LOGIN));
					user.setPassword(resultSet
							.getString(NamedConstant.DB_USER_PASSWORD));
					user.setRole(resultSet
							.getString(NamedConstant.DB_USER_ROLE));
					user.setName(resultSet
							.getString(NamedConstant.DB_USER_NAME));
					user.setSurname(resultSet
							.getString(NamedConstant.DB_USER_SURNAME));
					user.setTelephoneNumber(resultSet
							.getString(NamedConstant.DB_USER_TELNUMBER));
					user.setEmail(resultSet
							.getString(NamedConstant.DB_USER_EMAIL));
					user.setDiscount(resultSet
							.getDouble(NamedConstant.DB_USER_DISCOUNT));

				}

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return user;

	}

	@Override
	public boolean delete(int id) {
		boolean isDeleted = false;

		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_DELETE);

			statement.setInt(1, id);
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isDeleted = true;

		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isDeleted;
	
	}

	@Override
	public boolean update(User entity) {
		boolean isUpdated = false;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_UPDATE);

			statement.setString(1, entity.getLogin());
			statement.setString(2, entity.getPassword());
			statement.setString(3, entity.getRole());
			statement.setString(4, entity.getName());
			statement.setString(5, entity.getSurname());
			statement.setString(6, entity.getTelephoneNumber());
			statement.setString(7, entity.getEmail());
			statement.setDouble(8, entity.getDiscount());
			statement.setInt(9, entity.getId());
			
			statement.executeUpdate();
			ConnectionPool.getInstance().closeConnection(connection, statement);
			isUpdated = true;
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return isUpdated;
	}


	// @Override
	public String isLoginOccupied(String login) {
		String checkLogin = null;
		try {

			Connection connection = ConnectionPool.getInstance()
					.takeConnection();

			PreparedStatement statement = connection
					.prepareStatement(Query.USER_FIND_BY_LOGIN);

			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {

				checkLogin = resultSet.getString(1);

			}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return checkLogin;
	}

	@Override
	public User checkLogin(String login, String password) {
		User entity = null;
		try {
			
			Connection connection = ConnectionPool.getInstance()
					.takeConnection();
			PreparedStatement statement = connection
					.prepareStatement(Query.USER_CHECK_LOGIN);

			statement.setString(1, login);
			statement.setString(2, password);
			ResultSet resultSet = statement.executeQuery();

			if (!resultSet.wasNull()) { // обязательно переделать
											// проверку!!!!!!!!!!!!!!
				while (resultSet.next()) {

					entity = new User();
					entity.setId(resultSet.getInt(NamedConstant.DB_USER_ID));
					entity.setLogin(resultSet
							.getString(NamedConstant.DB_USER_LOGIN));
					entity.setPassword(resultSet.getString(NamedConstant.DB_USER_PASSWORD));
					entity.setRole(resultSet
							.getString(NamedConstant.DB_USER_ROLE));
					entity.setName(resultSet
							.getString(NamedConstant.DB_USER_NAME));
					entity.setSurname(resultSet
							.getString(NamedConstant.DB_USER_SURNAME));
					entity.setTelephoneNumber(resultSet
							.getString(NamedConstant.DB_USER_TELNUMBER));
					entity.setEmail(resultSet
							.getString(NamedConstant.DB_USER_EMAIL));
					entity.setDiscount(resultSet
							.getDouble(NamedConstant.DB_USER_DISCOUNT));

				}
		}
			ConnectionPool.getInstance().closeConnection(connection, statement,
					resultSet);
		} catch (SQLException e) {
			logger.error("Problem in connection with data base ", e);

		} catch (ConnectionPoolException e) {
			logger.error("Problem in connection with data base  ", e);
		}

		return entity;
	}

}
