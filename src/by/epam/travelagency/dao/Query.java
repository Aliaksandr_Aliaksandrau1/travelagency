package by.epam.travelagency.dao;

public class Query {
	
	public static final String USER_FIND_ALL = "SELECT usr_id, usr_login, usr_password, usr_role, usr_name, usr_surname, usr_telnumber, usr_email, usr_discount  FROM usr";
	public static final String USER_CREATE = "INSERT INTO usr (usr_login, usr_password, usr_role, usr_name, usr_surname, usr_telnumber, usr_email, usr_discount) VALUES(?,?,?,?,?,?,?,?)";
	public static final String USER_FIND_BY_LOGIN = "SELECT usr_login FROM usr WHERE login = '?'";
	public static final String USER_CHECK_LOGIN = "SELECT usr_id, usr_login, usr_password, usr_role, usr_name, usr_surname, usr_telnumber, usr_email,  usr_discount FROM usr WHERE usr_login = ? AND usr_password = ?";
	public static final String USER_FIND_BY_ID = "SELECT usr_id, usr_login, usr_password, usr_role, usr_name, usr_surname, usr_telnumber, usr_email, usr_discount  FROM usr WHERE usr_id = ?";
	public static final String USER_DELETE = "DELETE FROM usr WHERE usr_id = ?";
	public static final String USER_UPDATE = "UPDATE usr SET usr_login = ?, usr_password = ?, usr_role = ?, usr_name = ?, usr_surname = ?, usr_telnumber = ?, usr_email = ?, usr_discount = ? WHERE usr_id = ?";
	
	
	public static final String LOCATION_FIND_ALL = "SELECT loc_id, loc_country, loc_city, loc_hotel, loc_description FROM location";
	public static final String LOCATION_CREATE = "INSERT INTO location (loc_country, loc_city, loc_hotel, loc_description) VALUES(?,?,?,?)";
	public static final String LOCATION_DELETE = "DELETE FROM location WHERE loc_id = ?Q";
	public static final String LOCATION_UPDATE = "UPDATE location SET loc_country = ?, loc_city = ?, loc_hotel = ?, loc_description = ? WHERE loc_id = ?";
	public static final String LOCATION_FIND_BY_ID = "SELECT loc_id, loc_country, loc_city, loc_hotel, loc_description FROM location WHERE loc_id = ?";
	
	
	public static final String TOUR_FIND_ALL = "SELECT	tr_id, tr_title, tr_begin_date, tr_end_date, tt_id, tt_name, ts_id, ts_name, tr_description, tr_price FROM tour JOIN tour_status ON tr_ts_id = ts_id JOIN tour_type ON tr_tt_id = tt_id ORDER BY tr_id";
	public static final String TOUR_FIND_BY_ID = "SELECT tr_id, tr_title, tr_begin_date, tr_end_date, tt_id, tt_name, ts_id, ts_name, tr_description, tr_price FROM tour JOIN tour_status ON tr_ts_id = ts_id JOIN tour_type ON tr_tt_id = tt_id WHERE tr_id = ?";
	public static final String TOUR_CREATE = "INSERT INTO tour (tr_title, tr_begin_date, tr_end_date, tr_tt_id, tr_price, tr_ts_id, tr_description) VALUES(?,?,?,?,?,?,?)";
	public static final String TOUR_DELETE = "DELETE FROM tour WHERE tr_id = ?";
	public static final String TOUR_UPDATE = "UPDATE tour SET tr_title=?, tr_begin_date=?, tr_end_date=?, tr_tt_id=?, tr_price=?, tr_ts_id=?, tr_description=? WHERE tr_id = ?";
	
	
	public static final String TOURTYPE_FIND_ALL = "SELECT tt_id, tt_name FROM tour_type";
	public static final String TOURTYPE_CREATE = "INSERT INTO tour_type (tt_name) VALUES(?)";
	public static final String TOURTYPE_DELETE = "DELETE FROM tour_type WHERE tt_id = ?";
	public static final String TOURTYPE_UPDATE = "UPDATE tour_type SET tt_name = ? WHERE tt_id = ?";
	
	
	public static final String TOURSTATUS_FIND_ALL = "SELECT ts_id, ts_name FROM tour_status";
	public static final String TOURSTATUS_CREATE = "INSERT INTO tour_status (ts_name) VALUES(?)";
	public static final String TOURSTATUS_DELETE = "DELETE FROM tour_status WHERE ts_id = ?";
	public static final String TOURSTATUS_UPDATE = "UPDATE tour_status SET ts_name = ? WHERE ts_id = ?";

	
	
	public static final String ORDERSTATUS_FIND_ALL = "SELECT os_id, os_name FROM ordr_status";
	public static final String ORDERSTATUS_CREATE = "INSERT INTO ordr_status (os_name) VALUES(?)";
	public static final String ORDERSTATUS_DELETE = "DELETE FROM ordr_status WHERE os_id = ?";
	public static final String ORDERSTATUS_UPDATE = "UPDATE ordr_status SET os_name = ? WHERE os_id = ?";
	public static final String ORDERSTATUS_FIND_BY_ID = "SELECT os_id, os_name FROM ordr_status WHERE os_id = ?";
	
	
	
	public static final String LOCATION2TOUR_FIND_ALL = "SELECT lt_id, lt_loc_id, lt_tr_id FROM location2tour";
	public static final String LOCATION2TOUR_FIND_LOCATION_BY_TOUR_ID = "SELECT lt_tr_id, lt_id, lt_loc_id FROM location2tour WHERE lt_tr_id = ?";
	
	
	public static final String ORDER_FIND_ALL = "SELECT	ord_id, tr_id, tr_title, tr_begin_date, tr_end_date, tr_price, tr_description, os_id, os_name, usr_id, usr_login, usr_name, usr_surname, usr_telnumber, usr_email, usr_discount FROM ordr JOIN tour ON ord_tr_id = tr_id  JOIN ordr_status ON ord_os_id = os_id JOIN usr ON ord_usr_id = usr_id";
	public static final String ORDER_CREATE = "INSERT INTO ordr (ord_usr_id, ord_tr_id, ord_os_id) VALUES(?,?,?)";
	public static final String ORDER_DELETE = "DELETE FROM ordr WHERE ord_id = ?";
	public static final String ORDER_FIND_BY_ID = "SELECT	ord_id, tr_id, tr_title, tr_begin_date, tr_end_date, tr_price, tr_description, os_id, os_name, usr_id, usr_login, usr_name, usr_surname, usr_telnumber, usr_email, usr_discount FROM ordr JOIN tour ON ord_tr_id = tr_id  JOIN ordr_status ON ord_os_id = os_id JOIN usr ON ord_usr_id = usr_id WHERE ord_id = ?";
	
}
