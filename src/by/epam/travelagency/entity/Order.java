package by.epam.travelagency.entity;


public class Order extends Entity {
	private static final long serialVersionUID = 1L;

	private User user;
	private Tour tour;
	private OrderStatus orderStatus;

	public Order() {

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Tour getTour() {
		return tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public String toString() {
		return "Order [user=" + user + ", tour=" + tour + ", orderStatus="
				+ orderStatus + ", getId()=" + getId() + "]";
	}

	

}
