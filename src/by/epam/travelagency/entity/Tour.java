package by.epam.travelagency.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tour extends Entity {

	private static final long serialVersionUID = 1L;
	private String title;
	private Date beginDate;
	private Date endDate;
	private double price;
	private String description;
	private TourType tourType;
	private TourStatus tourStatus;
	
	
	//////////////
	
	private Map<Integer, Location> locationsInTourMap = new HashMap<Integer, Location>();
	
	

	public Map<Integer, Location> getLocationsInTourMap() {
		return locationsInTourMap;
	}

	public void setLocationsInTourMap (Map<Integer, Location> locationsInTourMap) {
		this.locationsInTourMap = locationsInTourMap;
	}
	
	
	
	////////////////////////////////
	
	private List<Location> locList = new ArrayList<Location>();
	public List<Location> getLocList() {
		return locList;
	}

	public void setLocList(List<Location> locList) {
		this.locList = locList;
	}

	
///////////////////
	
	
	

	public Tour() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TourType getTourType() {
		return tourType;
	}

	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}

	public TourStatus getTourStatus() {
		return tourStatus;
	}

	public void setTourStatus(TourStatus tourStatus) {
		this.tourStatus = tourStatus;
	}

	@Override
	public String toString() {
		return "Tour [title=" + title + ", beginDate=" + beginDate
				+ ", endDate=" + endDate + ", price=" + price
				+ ", description=" + description + ", tourType=" + tourType
				+ ", tourStatus=" + tourStatus + ", locationsInTourMap="
				+ locationsInTourMap + ", locList=" + locList + "]";
	}

	
	

}
