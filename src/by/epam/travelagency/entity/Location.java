package by.epam.travelagency.entity;

public class Location extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5355486234988801289L;
	//private static final long serialVersionUID = 1L;
	private String country;
	private String city;
	private String hotel;
	private String description;
	
	public Location() {

	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHotel() {
		return hotel;
	}

	public void setHotel(String hotel) {
		this.hotel = hotel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Location [country=" + country + ", city=" + city + ", hotel="
				+ hotel + ", description=" + description + ", id="
				+ getId() + "]";
	}
	
}
