package by.epam.travelagency.entity;

public class Location2Tour extends Entity {

	private static final long serialVersionUID = 1L;
	
	private int locationId;
	private int tourId;
	

	public Location2Tour() {
	}


	public int getLocationId() {
		return locationId;
	}


	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}


	public int getTourId() {
		return tourId;
	}


	public void setTourId(int tourId) {
		this.tourId = tourId;
	}

	
	
	
	/*private Tour tour;

	private Location location;
	
	
	
	public Tour getTour() {
		return tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}*/

}
