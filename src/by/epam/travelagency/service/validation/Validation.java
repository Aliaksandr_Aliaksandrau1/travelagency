package by.epam.travelagency.service.validation;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.travelagency.entity.Entity;
	public abstract class Validation <T extends Entity>   {
			
	public abstract boolean isInputParameterMapValid (Map<String, String> entityParameterMap);
	
	protected boolean checkData (String data, String regex) { 
		boolean isChecked = false;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(data);
		if (matcher.matches()) {
			isChecked = true;
		}

		return isChecked;
	}
	protected boolean checkData (double data, String regex) {
		Double dataDouble = new Double(data);
		String dataString = dataDouble.toString();
		boolean isChecked = false;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(dataString);
		if (matcher.matches()) {
			isChecked = true;
		}
		return isChecked;
	}
}
