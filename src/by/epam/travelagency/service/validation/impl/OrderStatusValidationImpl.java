package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.resource.ValidationRegExManager;
import by.epam.travelagency.service.validation.Validation;

public class OrderStatusValidationImpl extends Validation <OrderStatus> {

	private OrderStatusValidationImpl() {
	}
	public final static OrderStatusValidationImpl instance = new OrderStatusValidationImpl();

	public static OrderStatusValidationImpl getInstance() {
		return instance;
	}

	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
	
		boolean isChecked = false;
		String name = entityParameterMap.get("name");
		
		if (checkData(name, ValidationRegExManager
				.getProperty("validation.regex.orderstatus.name"))) {
			isChecked = true;
		}
	
		return isChecked;
	}

}
