package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.resource.ValidationRegExManager;
import by.epam.travelagency.service.validation.Validation;

public class UserValidationImpl extends Validation<User> {

	private UserValidationImpl() {
	}

	public final static UserValidationImpl instance = new UserValidationImpl();

	public static UserValidationImpl getInstance() {
		return instance;
	}

	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		boolean isChecked = false;
		String login = entityParameterMap.get("login");
		String password = entityParameterMap.get("password");
		String role = entityParameterMap.get("role");
		String name = entityParameterMap.get("name");
		String surname = entityParameterMap.get("surname");
		String telnumber = entityParameterMap.get("telnumber");
		String email = entityParameterMap.get("email");
		String discount = entityParameterMap.get("discount");

		String regexLogin = ValidationRegExManager.getProperty(
				"validation.regex.user.login").trim();
		String regexPassword = ValidationRegExManager.getProperty(
				"validation.regex.user.password").trim();
		String regexRole = ValidationRegExManager.getProperty(
				"validation.regex.user.role").trim();
		String regexName = ValidationRegExManager.getProperty(
				"validation.regex.user.name").trim();
		String regexSurname = ValidationRegExManager.getProperty(
				"validation.regex.user.surname").trim();
		String regexTelnumber = ValidationRegExManager.getProperty(
				"validation.regex.user.telnumber").trim();
		String regexEmail = ValidationRegExManager.getProperty(
				"validation.regex.user.email").trim();
		String regexDiscount = ValidationRegExManager.getProperty(
				"validation.regex.user.discount").trim();

		if (checkData(login, regexLogin) && checkData(password, regexPassword)
				&& checkData(role, regexRole) && checkData(name, regexName)
				&& checkData(surname, regexSurname)
				&& checkData(telnumber, regexTelnumber)
				&& checkData(email, regexEmail)
				&& checkData(discount, regexDiscount)

		) {

			isChecked = true;
		}

		return isChecked;
	}

}
