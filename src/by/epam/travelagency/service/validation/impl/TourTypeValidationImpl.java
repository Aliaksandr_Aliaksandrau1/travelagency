package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.resource.ValidationRegExManager;
import by.epam.travelagency.service.validation.Validation;

public class TourTypeValidationImpl extends Validation<TourType> {
	private TourTypeValidationImpl() {
	}
	private final static TourTypeValidationImpl instance = new TourTypeValidationImpl();

	public static TourTypeValidationImpl getInstance() {
		return instance;
	}
	

	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		boolean isChecked = false;
		
		String name = entityParameterMap.get("name");
		
		if (checkData(name, ValidationRegExManager
				.getProperty("validation.regex.tourtype.name"))) {
			isChecked = true;
		}
		
		return isChecked;
	}


	/*public boolean isEntityValid(TourType entity) {
		boolean isChecked = false;

		if (checkData(entity.getName(),
				ValidationRegExManager
						.getProperty("validation.regex.tourtype.name"))) {
			isChecked = true;
		}
		return isChecked;
	}*/
	
	
}
