package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.Order;
import by.epam.travelagency.service.resource.ValidationRegExManager;
import by.epam.travelagency.service.validation.Validation;

public class OrderValidationImpl extends Validation<Order> {

	private OrderValidationImpl() {
	}
	private final static OrderValidationImpl instance = new OrderValidationImpl();
	
	public static OrderValidationImpl getInstance() {
		return instance;
	}
	
	
	public boolean isEntityValid(Order entity) {
		return false;
	}  // �������

	
	
	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		boolean isChecked = false;
		String userId = entityParameterMap.get("userId");
		String tourId = entityParameterMap.get("tourId");
		String orderStatusId = entityParameterMap.get("orderStatusId");

		if (checkData(userId,
				ValidationRegExManager.getProperty("validation.regex.user.id"))
				&& checkData(tourId,
						ValidationRegExManager
								.getProperty("validation.regex.tour.id"))
				&& checkData(orderStatusId,
						ValidationRegExManager
								.getProperty("validation.regex.orderstatus.id"))) {
			isChecked = true;
		}

		return isChecked;
	}

}
