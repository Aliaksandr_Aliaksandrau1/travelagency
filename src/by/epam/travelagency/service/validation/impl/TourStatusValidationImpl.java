package by.epam.travelagency.service.validation.impl;

import java.util.Map;

import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.resource.ValidationRegExManager;
import by.epam.travelagency.service.validation.Validation;


public class TourStatusValidationImpl extends Validation<TourStatus> {
		
	private TourStatusValidationImpl() {
	}
	private final static TourStatusValidationImpl instance = new TourStatusValidationImpl();

	public static TourStatusValidationImpl getInstance() {
		return instance;
	}
	
	@Override
	public boolean isInputParameterMapValid(
			Map<String, String> entityParameterMap) {
		boolean isChecked = false;
		
		String name = entityParameterMap.get("name");
		
		if (checkData(name, ValidationRegExManager
				.getProperty("validation.regex.tourstatus.name"))) {
			isChecked = true;
		}
		
		return isChecked;
	}

	
}
