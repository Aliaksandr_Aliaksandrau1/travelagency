package by.epam.travelagency.service.validation;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateFormatValidationService {
	
	private DateFormatValidationService() {
	}

	private final static DateFormatValidationService instance = new DateFormatValidationService();

	public static DateFormatValidationService getInstance() {
		return instance;
	}

	
	public Date dateFormatterFromStringToDate (String dateString) {											
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(dateString, formatter);
		Date dateDate = Date.from(date1.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return dateDate;
	}
	
	
}
