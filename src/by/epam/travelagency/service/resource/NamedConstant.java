package by.epam.travelagency.service.resource;

public class NamedConstant {
	
	/** 
	 * |||||||||||||||||||||||||| JSP Form Constants ||||||||||||||||||||||
	 */
	
 	
	/** 
	 * USER
	 */
	public static final String PARAM_USER_ID = "id";
	public static final String PARAM_USER_LOGIN = "login";					
	public static final String PARAM_USER_PASSWORD = "password";
	public static final String PARAM_USER_ROLE = "role";
	public static final String PARAM_USER_NAME = "name";
	public static final String PARAM_USER_SURNAME = "surname";
	public static final String PARAM_USER_TELNUMBER = "telnumber";
	public static final String PARAM_USER_EMAIL = "email";
	public static final String PARAM_USER_DISCOUNT = "discount";
	
	
	
	public static final String PARAM_USER_ROLE_BY_DEFAULT = "guest";
	public static final String PARAM_SESSION_LOCAL = "local";
	public static final String PARAM_SESSION_LOCAL_BY_DEFAULT = "en";
	
	
	
	
	/**
	 * LOCATION
	 */
	public static final String PARAM_LOCATION_ID = "id";
	public static final String PARAM_LOCATION_COUNTRY = "country";
	public static final String PARAM_LOCATION_CITY = "city";
	public static final String PARAM_LOCATION_HOTEL = "hotel";
	public static final String PARAM_LOCATION_DESCRIPTION = "description";
	
	/**
	 * TOUR TYPE
	 */
	public static final String PARAM_TOURTYPE_ID = "id";
	public static final String PARAM_TOURTYPE_NAME = "name";
	/**
	 * TOUR STATUS
	 */
	public static final String PARAM_TOURSTATUS_ID = "id";
	public static final String PARAM_TOURSTATUS_NAME = "name";
	
	/** 
	
	 * TOUR STATUS
	 */
	public static final String PARAM_ORDERSTATUS_ID = "id";
	public static final String PARAM_ORDERSTATUS_NAME = "name";
	
	
	
	/*
	 * TOUR
	 */
	
	public static final String PARAM_TOUR_ID = "id";
	public static final String PARAM_TOUR_TITLE = "title";
	public static final String PARAM_TOUR_BEGIN_DATE = "begin_date";
	public static final String PARAM_TOUR_END_DATE = "end_date";
	public static final String PARAM_TOUR_TOURTYPE = "tourtype";
	public static final String PARAM_TOUR_TOURSTATUS = "tourstatus";
	public static final String PARAM_TOUR_PRICE = "price";
	public static final String PARAM_TOUR_DESCRIPTION = "description";
	
	public static final String PARAM_TOUR_LOCATION = "location";
	
	
	/*
	 * ORDER
	 */
	
	public static final String PARAM_ORDER_ID = "id";
	public static final String PARAM_ORDER_TOUR_ID = "tourId";
	public static final String PARAM_ORDER_USER_ID = "userId";
	public static final String PARAM_ORDER_ORDER_STATUS_ID = "orderStatusId";

	
	
	
	/** 
	 * ||||||||||||||||||||||| Database constants |||||||||||||||||||||||||||||||||
	 * 
	 */
	
	/** 
	 * USER
	 */
	
	
	public static final String DB_USER_ID = "usr_id";
	public static final String DB_USER_LOGIN = "usr_login";
	public static final String DB_USER_PASSWORD = "usr_password";
	public static final String DB_USER_ROLE = "usr_role";
	public static final String DB_USER_NAME = "usr_name";
	public static final String DB_USER_SURNAME = "usr_surname";
	public static final String DB_USER_TELNUMBER = "usr_telnumber";
	public static final String DB_USER_EMAIL = "usr_email";
	public static final String DB_USER_DISCOUNT = "usr_discount";
	
	
	
	/**
	 * LOCATION
	 */
	public static final String DB_LOCATION_ID = "loc_id";
	public static final String DB_LOCATION_COUNTRY = "loc_country";
	public static final String DB_LOCATION_CITY = "loc_city";
	public static final String DB_LOCATION_HOTEL = "loc_hotel";
	public static final String DB_LOCATION_DESCRIPTION = "loc_description";

	
	
	/**
	 * TOUR_TYPE
	 */
	public static final String DB_TOURTYPE_ID = "tt_id";
	public static final String DB_TOURTYPE_NAME = "tt_name";
	
	/**
	 * TOUR_STATUS
	 */
	public static final String DB_TOURSTATUS_ID = "ts_id";
	public static final String DB_TOURSTATUS_NAME = "ts_name";
	
	
	/*
	 * TOUR
	 */
	
	public static final String DB_TOUR_ID = "tr_id";
	public static final String DB_TOUR_TITLE = "tr_title";
	public static final String DB_TOUR_BEGIN_DATE = "tr_begin_date";
	public static final String DB_TOUR_END_DATE = "tr_end_date";
	public static final String DB_TOUR_TOURTYPE_ID = "tr_tt_id";
	public static final String DB_TOUR_PRICE = "tr_price";
	public static final String DB_TOUR_TOURSTATUS_ID = "tr_ts_id";
	public static final String DB_TOUR_DESCRIPTION = "tr_description";
	

	/*
	 * LOCATION2TOUR
	 */
	public static final String DB_LOCATION2TOUR_ID = "lt_id";
	public static final String DB_LOCATION2TOUR_LOCATION_ID = "lt_loc_id";
	public static final String DB_LOCATION2TOUR_TOUR_ID = "lt_tr_id";
	
	
	/**
	 * ORDER
	 */
	
	public static final String DB_ORDER_ID = "ord_id";
	public static final String DB_ORDER_USER_ID = "ord_usr_id";
	public static final String DB_ORDER_TOUR_ID = "ord_tr_id";
	public static final String DB_ORDER_ORDERSTATUS_ID = "ord_os_id";
	
	
	/**
	 * ORDER_STATUS
	 */
	
	public static final String DB_ORDERSTATUS_ID = "os_id";
	public static final String DB_ORDERSTATUS_NAME = "os_name";
	
}
