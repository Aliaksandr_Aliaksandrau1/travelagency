package by.epam.travelagency.service.resource;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleManager {

	private LocaleManager() {
	}

	private final static LocaleManager instance = new LocaleManager();

	public static LocaleManager getInstance() {
		return instance;
	}

	// �������� �����
	
	public String getMessage(String key, String loc) {
		String value = "";  // ����������
		Locale locale = new Locale(loc);
		ResourceBundle bundle = ResourceBundle.getBundle("resources.locale",
				locale);

		bundle = ResourceBundle.getBundle("resources.locale", locale);
		if (bundle.containsKey(key)) {
			value = bundle.getString(key);
		}

		return value;
	}

	/*
	 * class ResourceProperty { private ResourceBundle bundle;
	 * 
	 * public ResourceProperty(Locale locale) { bundle = ResourceBundle
	 * .getBundle("resources.prop", locale); }
	 * 
	 * public String getValue(String key) { return bundle.getString(key); } }
	 * 
	 * public String getMessage1 (String key, String loc) { String value = null;
	 * 
	 * ResourceProperty myBundle = new ResourceProperty(new Locale(loc)); value
	 * = myBundle.getValue(key);
	 * 
	 * System.out.println(loc + "   - locale"); System.out.println(value +
	 * "   - in Manager");
	 * 
	 * return value; }
	 */

}
