package by.epam.travelagency.service.exception;

import by.epam.travelagency.exception.TravelAgencyProjectException;

public class ServiceException extends TravelAgencyProjectException {
	private static final long serialVersionUID = 1L;

	public ServiceException() {

	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
