package by.epam.travelagency.service.entityservice;

import java.util.Map;

import by.epam.travelagency.entity.Entity;

public interface EntityBuilderService <T extends Entity> {
	public T buildEntity (Map <String, String> entityParameterMap);
}
