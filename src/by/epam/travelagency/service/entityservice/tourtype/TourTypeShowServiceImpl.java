package by.epam.travelagency.service.entityservice.tourtype;

import java.util.List;

import by.epam.travelagency.dao.impl.TourTypeDAOImpl;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.IEntityShowService;

public class TourTypeShowServiceImpl implements IEntityShowService<TourType> {
	private TourTypeShowServiceImpl() {
	}

	private final static TourTypeShowServiceImpl instance = new TourTypeShowServiceImpl();
	public static TourTypeShowServiceImpl getInstance() {
		return instance;
	}

	@Override
	public List<TourType> entityShow() {
		List <TourType> entityList = TourTypeDAOImpl.getInstance().findAll();
		return entityList;
	}

}
