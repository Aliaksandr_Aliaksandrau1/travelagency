package by.epam.travelagency.service.entityservice.tourtype;

import java.util.Map;

import by.epam.travelagency.dao.impl.TourTypeDAOImpl;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.IEntityUpdateService;
import by.epam.travelagency.service.validation.impl.TourTypeValidationImpl;

public class TourTypeUpdateServiceImpl implements IEntityUpdateService <TourType>{

	private TourTypeUpdateServiceImpl() {
	}
	
	private final static TourTypeUpdateServiceImpl instance = new TourTypeUpdateServiceImpl();

	public static TourTypeUpdateServiceImpl getInstance() {
		return instance;
	}

	@Override
	public TourType entityUpdate(Map<String, String> entityParameterMap) {
		TourType entity = null;

		if (TourTypeValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new TourType();
			int id = Integer.parseInt(entityParameterMap.get("id"));
			String name = entityParameterMap.get("name");
			entity.setId(id);
			entity.setName(name);

			if (!TourTypeDAOImpl.getInstance().update(entity)) {
				entity = null;
			}

		}
		return entity;
	}
}
