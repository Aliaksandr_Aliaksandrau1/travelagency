package by.epam.travelagency.service.entityservice.tourtype;


import java.util.Map;

import by.epam.travelagency.dao.impl.TourTypeDAOImpl;
import by.epam.travelagency.entity.TourType;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.validation.impl.TourTypeValidationImpl;

public class TourTypeCreateServiceImpl implements IEntityCreateService <TourType>{

	private TourTypeCreateServiceImpl() {
	}
	private final static TourTypeCreateServiceImpl instance = new TourTypeCreateServiceImpl();

	public static TourTypeCreateServiceImpl getInstance() {
		return instance;
	}


	@Override
	public TourType entityCreateNew ( Map<String, String> entityParameterMap) {
		TourType entity = null;
		if (TourTypeValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new TourType();
			String name = entityParameterMap.get("name");
			entity.setName(name);
			if (!TourTypeDAOImpl.getInstance().create(entity)) {
				entity = null;
			}
		}

		return entity;
	}

}
