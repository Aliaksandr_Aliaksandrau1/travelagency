package by.epam.travelagency.service.entityservice.orderstatus;

import java.util.Map;

import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.validation.impl.OrderStatusValidationImpl;

public class OrderStatusCreateServiceImpl implements
		IEntityCreateService<OrderStatus> {

	private OrderStatusCreateServiceImpl() {
	}

	private final static OrderStatusCreateServiceImpl instance = new OrderStatusCreateServiceImpl();

	public static OrderStatusCreateServiceImpl getInstance() {
		return instance;
	}

	@Override
	public OrderStatus entityCreateNew (Map<String, String> entityParameterMap) { 

		OrderStatus entity = null;
		if (OrderStatusValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new OrderStatus();
			String name = entityParameterMap.get("name");
			entity.setName(name);
			if (!OrderStatusDAOImpl.getInstance().create(entity)) {
				entity = null;
			}
		}

		return entity;
	}

}
