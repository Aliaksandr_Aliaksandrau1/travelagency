package by.epam.travelagency.service.entityservice;

import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.service.exception.ServiceException;

public interface IEntityUpdateGoService<T extends Entity> {

	public T entityToUpdate (int id) throws ServiceException;

}
