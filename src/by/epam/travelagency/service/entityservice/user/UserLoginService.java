package by.epam.travelagency.service.entityservice.user;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;

public class UserLoginService {
	private UserLoginService() {
	}

	private final static UserLoginService instance = new UserLoginService();

	public static UserLoginService getInstance() {
		return instance;
	}

	public User checkLogin(String login, String password) {
		User user = null;
		user = UserDAOImpl.getInstance().checkLogin(login, password);
		
		
		
		
		return user;

	}

}
