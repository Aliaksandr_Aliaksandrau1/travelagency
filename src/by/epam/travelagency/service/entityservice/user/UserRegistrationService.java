package by.epam.travelagency.service.entityservice.user;



import java.util.Map;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.validation.impl.UserValidationImpl;

public class UserRegistrationService {

	private UserRegistrationService() {
	}

	private final static UserRegistrationService instance = new UserRegistrationService();

	public static UserRegistrationService getInstance() {
		return instance;
	}

	public User userRegistrate(Map<String, String> entityParameterMap) {
		User entity = null;
		
		
		
		if (UserValidationImpl.getInstance().isInputParameterMapValid(entityParameterMap)) {
			entity = new User();

			String login = entityParameterMap.get("login");
			String password = entityParameterMap.get("password");
			String role = entityParameterMap.get("role");
			String name = entityParameterMap.get("name");
			String surname = entityParameterMap.get("surname");
			String telnumber = entityParameterMap.get("telnumber");
			String email = entityParameterMap.get("email");
			double discount = Double.parseDouble(entityParameterMap.get("discount"));

			entity.setLogin(login);
			entity.setPassword(password);
			entity.setRole(role);
			entity.setName(name);
			entity.setSurname(surname);
			entity.setTelephoneNumber(telnumber);
			entity.setEmail(email);
			entity.setDiscount(discount);

			if (!UserDAOImpl.getInstance().create(entity)) {
				entity = null;
			}
		}

		return entity;
	}

}
