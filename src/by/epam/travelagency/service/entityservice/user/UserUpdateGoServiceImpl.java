package by.epam.travelagency.service.entityservice.user;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.IEntityUpdateGoService;

public class UserUpdateGoServiceImpl implements
		IEntityUpdateGoService<User> {

	private UserUpdateGoServiceImpl() {
	}

	private final static UserUpdateGoServiceImpl instance = new UserUpdateGoServiceImpl();

	public static UserUpdateGoServiceImpl getInstance() {
		return instance;
	}

	@Override
	public User entityToUpdate(int id) {
		User entity = null;
		entity = UserDAOImpl.getInstance().findEntityById(id);
		return entity;
	}

}
