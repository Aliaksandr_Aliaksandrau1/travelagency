package by.epam.travelagency.service.entityservice.user;

import java.util.List;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.IEntityShowService;

public class UserShowServiceImpl implements IEntityShowService<User> {
	private UserShowServiceImpl() {
	}

	private final static UserShowServiceImpl instance = new UserShowServiceImpl();

	public static UserShowServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public List<User> entityShow() {
		List<User> entityList = UserDAOImpl.getInstance().findAll();
		return entityList;
	}

}
