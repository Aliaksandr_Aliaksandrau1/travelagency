package by.epam.travelagency.service.entityservice.user;

import by.epam.travelagency.dao.impl.UserDAOImpl;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class UserDeleteServiceImpl implements IEntityDeleteService  {

	private UserDeleteServiceImpl() {
	}
	private final static UserDeleteServiceImpl instance = new UserDeleteServiceImpl();

	public static UserDeleteServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public boolean entityDeleteNew(int id) {
		
		return UserDAOImpl.getInstance().delete(id);
	}
	
}
