package by.epam.travelagency.service.entityservice;

import java.util.Map;

import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.service.exception.ServiceException;

public interface IEntityUpdateService<T extends Entity> {

	public T entityUpdate (Map <String, String> entityParameterMap) throws ServiceException;

}
