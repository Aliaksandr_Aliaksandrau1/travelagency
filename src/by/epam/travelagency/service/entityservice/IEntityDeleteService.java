package by.epam.travelagency.service.entityservice;

import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.service.exception.ServiceException;

public interface IEntityDeleteService   {
	//public  List <T> entityDelete(int id);
	public  boolean entityDeleteNew (int id) throws ServiceException;
}
