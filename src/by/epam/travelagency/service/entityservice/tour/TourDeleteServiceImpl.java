package by.epam.travelagency.service.entityservice.tour;

import java.util.List;

import by.epam.travelagency.dao.impl.TourDAOImpl;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class TourDeleteServiceImpl implements
		IEntityDeleteService {

	private TourDeleteServiceImpl() {
	}

	private final static TourDeleteServiceImpl instance = new TourDeleteServiceImpl();

	public static TourDeleteServiceImpl getInstance() {
		return instance;
	}

	
	public List<Tour> entityDelete(int id) {
		List<Tour> entityList = null;
		if (TourDAOImpl.getInstance().delete(id)) {

			entityList = TourShowServiceImpl.getInstance().entityShow(); // ��������� ����� ������� � ������� TourDelete
		}
		return entityList;
	}

	@Override
	public boolean entityDeleteNew(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
