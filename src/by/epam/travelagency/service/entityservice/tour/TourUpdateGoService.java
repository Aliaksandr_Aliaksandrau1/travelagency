package by.epam.travelagency.service.entityservice.tour;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.Location2TourDAOImpl;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.dao.impl.TourDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.entity.Location2Tour;
import by.epam.travelagency.entity.Tour;




public class TourUpdateGoService  {
	private TourUpdateGoService() {
	}

	private final static TourUpdateGoService instance = new TourUpdateGoService();

	public static TourUpdateGoService getInstance() {
		return instance;
	}
		
	
	public Tour entityGetById(int tourId) {
		//List<Tour> tourList;
	
		Tour entity = null;
		Location location = null;
		
		List<Location> locationList = null;
		List<Location2Tour> location2TourList = null;

		entity = TourDAOImpl.getInstance().findEntityById(tourId);
		
		
		
		
		try {
			locationList = LocationDAOImpl.getInstance().findAll();
		} catch (DAOException e) {
			// TODO Auto-generated catch block    // ����������
			e.printStackTrace();
		}
		
		
		
		
		location2TourList = Location2TourDAOImpl.getInstance().getLoc2TourListByTourId(tourId);

	
		Map <Integer, Location> locationMap = new HashMap <Integer, Location>();

		
		for (Location loc : locationList) {
			locationMap.put(loc.getId(), loc);
		}

		for (Location2Tour l2t : location2TourList) {  // ����� ���������� ������ location2TourList � ���������� ������ ������� ��� ���� � tourId
			
			location = locationMap.get(l2t.getLocationId());  
			
			
			entity.getLocationsInTourMap().put(location.getId(), location); // ��������� ������� � locationMap ����
	
		}

		return entity;
	}
}