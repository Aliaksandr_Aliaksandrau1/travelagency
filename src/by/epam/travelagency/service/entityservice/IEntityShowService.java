package by.epam.travelagency.service.entityservice;

import java.util.List;

import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.service.exception.ServiceException;

public interface IEntityShowService <T extends Entity> {
	public  List <T> entityShow() throws ServiceException;
}
