package by.epam.travelagency.service.entityservice.tourstatus;


import by.epam.travelagency.dao.impl.TourStatusDAOImpl;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class TourStatusDeleteServiceImpl implements IEntityDeleteService {

	private TourStatusDeleteServiceImpl() {
	}

	private final static TourStatusDeleteServiceImpl instance = new TourStatusDeleteServiceImpl();

	public static TourStatusDeleteServiceImpl getInstance() {
		return instance;
	}

		
	@Override
	public boolean entityDeleteNew(int id) {
		return TourStatusDAOImpl.getInstance().delete(id);
	}

}
