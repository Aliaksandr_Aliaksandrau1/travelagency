package by.epam.travelagency.service.entityservice.tourstatus;

import java.util.List;

import by.epam.travelagency.dao.impl.TourStatusDAOImpl;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.entityservice.IEntityShowService;

public class TourStatusShowServiceImpl implements IEntityShowService<TourStatus> {
	private TourStatusShowServiceImpl() {
	}

	private final static TourStatusShowServiceImpl instance = new TourStatusShowServiceImpl();
	public static TourStatusShowServiceImpl getInstance() {
		return instance;
	}

	@Override
	public List<TourStatus> entityShow() {
		List<TourStatus> entityList = null;
		entityList = TourStatusDAOImpl.getInstance().findAll();
		return entityList;
	}

}
