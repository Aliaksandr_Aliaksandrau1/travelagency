package by.epam.travelagency.service.entityservice.tourstatus;

import java.util.Map;

import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;
import by.epam.travelagency.dao.impl.TourStatusDAOImpl;
import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.TourStatus;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.entityservice.orderstatus.OrderStatusCreateServiceImpl;
import by.epam.travelagency.service.validation.impl.OrderStatusValidationImpl;
import by.epam.travelagency.service.validation.impl.TourStatusValidationImpl;

public class TourStatusCreateServiceImpl implements IEntityCreateService <TourStatus> {

	private TourStatusCreateServiceImpl() {
	}
	private final static TourStatusCreateServiceImpl instance = new TourStatusCreateServiceImpl();

	public static TourStatusCreateServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public TourStatus entityCreateNew(Map <String, String> entityParameterMap) {
		
		
		TourStatus entity = null;
		if (TourStatusValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new TourStatus();
			String name = entityParameterMap.get("name");
			entity.setName(name);
			if (!TourStatusDAOImpl.getInstance().create(entity)) {
				entity = null;
			}
		}
	
		return entity;
	}

}
