package by.epam.travelagency.service.entityservice;

import java.util.Map;

import by.epam.travelagency.entity.Entity;
import by.epam.travelagency.service.exception.ServiceException;

public interface IEntityCreateService <T extends Entity> {

	public T entityCreateNew(Map<String, String> entityParameterMap) throws ServiceException;

}
