package by.epam.travelagency.service.entityservice.order;

import java.util.Map;

import org.apache.log4j.Logger;

import by.epam.travelagency.dao.impl.OrderDAOImpl;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.entity.Tour;
import by.epam.travelagency.entity.User;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.validation.impl.OrderValidationImpl;

public class OrderCreateService implements IEntityCreateService<Order> {
	private final static Logger logger = Logger.getRootLogger();
	public OrderCreateService() {
	}

	
	public boolean entityCreate(Map<String, String> entityParameterMap) {
		boolean successEntityCreate = false;
		//OrderValidationImpl validator = new OrderValidationImpl();

		if (OrderValidationImpl.getInstance().isInputParameterMapValid(entityParameterMap)) {

			Order order = new Order();
			
			order.setTour(new Tour());
			order.setUser(new User());
			order.setOrderStatus(new OrderStatus());
			
			
			
			
			int tourId = Integer.parseInt(entityParameterMap.get("tourId"));
			int userId = Integer.parseInt(entityParameterMap.get("userId"));
			int orderStatusId = Integer.parseInt(entityParameterMap
					.get("orderStatusId"));

			
			
			
			
			
			order.getUser().setId(userId);
			order.getTour().setId(tourId);
			order.getOrderStatus().setId(orderStatusId);

			
			
			System.out.println(order);
			
			OrderDAOImpl.getInstance().create(order);
			successEntityCreate = true;

		}

		return successEntityCreate;

	}


	@Override
	public Order entityCreateNew(Map<String, String> entityParameterMap) {
		// TODO Auto-generated method stub
		return null;
	}
}
