package by.epam.travelagency.service.entityservice.order;

import java.util.List;

import by.epam.travelagency.dao.impl.OrderDAOImpl;
import by.epam.travelagency.dao.impl.OrderStatusDAOImpl;
import by.epam.travelagency.entity.Order;
import by.epam.travelagency.entity.OrderStatus;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;

public class OrderDeleteServiceImpl implements
		IEntityDeleteService {

	private OrderDeleteServiceImpl() {
	}

	private final static OrderDeleteServiceImpl instance = new OrderDeleteServiceImpl();

	public static OrderDeleteServiceImpl getInstance() {
		return instance;
	}

	
	public List<Order> entityDelete(int id) {
		List<Order> entityList = null;
		if (OrderDAOImpl.getInstance().delete(id)) {

			entityList = OrderShowService.getInstance().entityShow();
			
		}
		return entityList;
	}

	@Override
	public boolean entityDeleteNew(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
