package by.epam.travelagency.service.entityservice.location;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.IEntityUpdateGoService;
import by.epam.travelagency.service.exception.ServiceException;

public class LocationUpdateGoServiceImpl implements
		IEntityUpdateGoService<Location> {

	private LocationUpdateGoServiceImpl() {
	}

	private final static LocationUpdateGoServiceImpl instance = new LocationUpdateGoServiceImpl();

	public static LocationUpdateGoServiceImpl getInstance() {
		return instance;
	}

	@Override
	public Location entityToUpdate(int id) throws ServiceException{
		Location entity = null;
		
		try {
			entity = LocationDAOImpl.getInstance().findEntityById(id);
		} catch (DAOException e) {
			throw new  ServiceException(ExceptionMessage.ENTITYSERVICE_LOCATION_UPDATE_GO + e, e);
		}
		
		return entity;
	}

}
