package by.epam.travelagency.service.entityservice.location;

import java.util.Map;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.IEntityCreateService;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.validation.impl.LocationValidationImpl;


public class LocationCreateServiceImpl implements
		IEntityCreateService<Location> {

	private LocationCreateServiceImpl() {
	}

	private final static LocationCreateServiceImpl instance = new LocationCreateServiceImpl();

	public static LocationCreateServiceImpl getInstance() {
		return instance;
	}

	@Override
	public Location entityCreateNew (Map<String, String> entityParameterMap) throws ServiceException {
		Location entity = null;
		boolean isCreated = false;
		if (LocationValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new Location();

			String country = entityParameterMap.get("country");
			String city = entityParameterMap.get("city");
			String hotel = entityParameterMap.get("hotel");
			String description = entityParameterMap.get("description");

			entity.setCountry(country);
			entity.setCity(city);
			entity.setHotel(hotel);
			entity.setDescription(description);

			try {
				isCreated =  LocationDAOImpl.getInstance().create(entity);
			} catch (DAOException e) {
				throw new ServiceException(ExceptionMessage.ENTITYSERVICE_LOCATION_CREATE + e, e);
			}
		
			if (!isCreated) {
				entity = null;
			}
		}
		return entity;
	}

}
