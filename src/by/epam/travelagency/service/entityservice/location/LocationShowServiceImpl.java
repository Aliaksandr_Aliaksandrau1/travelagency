package by.epam.travelagency.service.entityservice.location;

import java.util.List;




import org.apache.log4j.Logger;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.IEntityShowService;
import by.epam.travelagency.service.exception.ServiceException;

public class LocationShowServiceImpl implements IEntityShowService<Location> {
	private final static Logger logger = Logger.getRootLogger();
	private LocationShowServiceImpl() {
	}

	private final static LocationShowServiceImpl instance = new LocationShowServiceImpl();

	public static LocationShowServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public List<Location> entityShow() throws ServiceException{
		List<Location> entityList = null;
		try {
			entityList = LocationDAOImpl.getInstance().findAll();
			
					
			
		} catch (DAOException e) {
			throw new  ServiceException(ExceptionMessage.ENTITYSERVICE_LOCATION_SHOW + e, e);
		}
		return entityList;
	}

}
