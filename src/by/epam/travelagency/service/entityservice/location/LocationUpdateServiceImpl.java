package by.epam.travelagency.service.entityservice.location;


import java.util.Map;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.entity.Location;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.IEntityUpdateService;
import by.epam.travelagency.service.exception.ServiceException;
import by.epam.travelagency.service.validation.impl.LocationValidationImpl;


public  class LocationUpdateServiceImpl implements
IEntityUpdateService<Location> {

	private LocationUpdateServiceImpl() {
	}
	private final static LocationUpdateServiceImpl instance = new LocationUpdateServiceImpl();

	public static LocationUpdateServiceImpl getInstance() {
		return instance;
	}
	

	@Override
	public Location entityUpdate(Map<String, String> entityParameterMap) throws ServiceException{
		boolean isUpdated = false;
		Location entity = null;

		if (LocationValidationImpl.getInstance().isInputParameterMapValid(
				entityParameterMap)) {
			entity = new Location();
			
			int id = Integer.parseInt(entityParameterMap.get("id"));
			String country = entityParameterMap.get("country");
			String city = entityParameterMap.get("city");
			String hotel = entityParameterMap.get("hotel");
			String description = entityParameterMap.get("description");
			
			entity.setId(id);
			entity.setCountry(country);
			entity.setCity(city);
			entity.setHotel(hotel);
			entity.setDescription(description);
			
			try {
				isUpdated = LocationDAOImpl.getInstance().update(entity);
			} catch (DAOException e) {
				throw new  ServiceException(ExceptionMessage.ENTITYSERVICE_LOCATION_UPDATE + e, e);
			}
			
			if (!isUpdated) {
				entity = null;
			}

		}
		return entity;
	}
}
