package by.epam.travelagency.service.entityservice.location;

import by.epam.travelagency.dao.DAOException;
import by.epam.travelagency.dao.impl.LocationDAOImpl;
import by.epam.travelagency.exception.ExceptionMessage;
import by.epam.travelagency.service.entityservice.IEntityDeleteService;
import by.epam.travelagency.service.exception.ServiceException;

public class LocationDeleteServiceImpl implements IEntityDeleteService  {

	private LocationDeleteServiceImpl() {
	}
	private final static LocationDeleteServiceImpl instance = new LocationDeleteServiceImpl();

	public static LocationDeleteServiceImpl getInstance() {
		return instance;
	}
	
	@Override
	public boolean entityDeleteNew(int id) throws ServiceException{
		boolean isDeleted = false;
		
		try {
			isDeleted = LocationDAOImpl.getInstance().delete(id);
		} catch (DAOException e) {
			throw new  ServiceException(ExceptionMessage.ENTITYSERVICE_LOCATION_DELETE + e, e);
		}
		return isDeleted;
		
	}
	
}
