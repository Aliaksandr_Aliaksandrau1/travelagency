<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Touragent page</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	
	<h1> Update status of order </h1>
	

<table>
		<tr>
			<th>ID</th>
			<th>Customer Login</th>
			<th>Customer Name</th>
			<th>Customer SurName</th>
			
			<th>Tour Title</th>
			<th>Tour Begin Date</th>
			<th>Tour End Date</th>
			<th>Price, $</th>
			
			<th>Order status</th>

		</tr>
			<tr>
				<td>${entity.id}</td>
				<td>${entity.user.login}</td>
				<td>${entity.user.name}</td>
				<td>${entity.user.surname}</td>
			
				<td>${entity.tour.title}</td>
				<td>${entity.tour.beginDate}</td>
				<td>${entity.tour.endDate}</td>
				<td>${entity.tour.price}</td>
				
				<td>${entity.orderStatus.name}</td>

				
			</tr>
		</table>

	<form method="POST" action="Controller">
		<input type="hidden" name="command" value="order_update" />
			
			<select name="orderstatus">
				<c:forEach var="variable" items="${orderStatusList}">
						<option value="${variable.id}"> ${variable.name} </option>
				</c:forEach> 
			</select>	Choose order status:
		
			<input type="submit" value="Update order" />
	</form>


</body>
</html>