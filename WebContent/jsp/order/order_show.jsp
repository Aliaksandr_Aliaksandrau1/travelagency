<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tour</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">

</head>
<body>


	<h1>Order Catalogue</h1>

	<table>
		<tr>
			<th>ID</th>
			<th>Customer Login</th>
			<th>Customer Name</th>
			<th>Customer SurName</th>
			
			<th>Tour Title</th>
			<th>Tour Begin Date</th>
			<th>Tour End Date</th>
			<th>Price, $</th>
			
			<th>Order status</th>

		</tr>
		<c:forEach var="variable" items="${orderList}">
		
			<tr>
				<td>${variable.id}</td>
				<td>${variable.user.login}</td>
				<td>${variable.user.name}</td>
				<td>${variable.user.surname}</td>
				
				<td>${variable.tour.title}</td>
				<td>${variable.tour.beginDate}</td>
				<td>${variable.tour.endDate}</td>
				<td>${variable.tour.price}</td>
				
				<td>${variable.orderStatus.name}</td>
				
				<td>
					<table>
						<tr>
							<th>
								<form method="POST" action="Controller">
									<input type="hidden" name="command" value="order_delete" />
									<input type="hidden" name="id" value="${variable.id}">
									<input type="submit" value="Delete Order" />
								</form>
							</th>
							
							<th>
								<form method="POST" action="Controller">
									<input type="hidden" name="command"
										value="order_update_go" /> <input type="hidden"
										name="id" value="${variable.id}"> <input type="hidden"
										name="orderStatusId" value="${variable.orderStatus.id}">

									<input type="submit" value="Change OrderStatus" />
								</form>

							</th>
						</tr>
					</table>

				</td>
				
				
			</tr>
		</c:forEach>
	</table>

	<br />

	<table>
		<tr>
			
			
			<th><a href="Controller?command=order_show">Refresh
					List</a></th>
		</tr>
	</table>

				
</body>
</html>