<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Touragent page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">

</head>
<body>

	<h1>Touragent Show Location Page</h1>

	<table>
		<tr>
			<th>ID</th>
			<th>Country</th>
			<th>City</th>
			<th>Hotel</th>
			<th>Description</th>
			<th>Action</th>

		</tr>
		<c:forEach var="movie" items="${locationList}">
			<tr>
				<td>${movie.id}</td>
				<td>${movie.country}</td>
				<td>${movie.city}</td>
				<td>${movie.hotel}</td>
				<td>${movie.description}</td>
				<td>
					<table>
						<tr>
							<th>
								<form name="locationDeleteForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="location_delete" />
									<input type="hidden" name="id" value="${movie.id}"> <input
										type="submit" value="Delete" />
								</form>
							</th>
							<th>

								<form name="locationUpdateForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="location_update_go" />
									<input type="hidden" name="id" value="${movie.id}"> <input
										type="submit" value="Update" />
								</form>

							</th>
						</tr>
					</table>

				</td>

			</tr>
		</c:forEach>


	</table>
	<br /> ${locationDeleteErrorMessage} ${locationDeleteSuccessMessage}
	${locationShowErrorMessage}
	<br />
	<table>
		<tr>
			<th><a href="Controller?command=location_create_go">Create
					location</a></th>
			<th><a href="Controller?command=location_show">Refresh List</a></th>
		</tr>
	</table>

</body>
</html>