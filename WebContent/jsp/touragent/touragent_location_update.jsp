<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Touragent page</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	
	<h1>Touragent: Update location</h1>
	<form name="UpdateLocationForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="location_update" />
			<input type="hidden" name="id" value="<c:if test="${not empty entity}">${entity.id}</c:if>" /> 
			<input type="text" name="country" value="<c:if test="${not empty entity}">${entity.country}</c:if>" /> Enter country*: <br />
			<input type="text" name="city" value="<c:if test="${not empty entity}">${entity.city}</c:if>" /> Enter city*: <br />
			<input type="text" name="hotel" value="<c:if test="${not empty entity}">${entity.hotel}</c:if>" /> Enter hotel name *: <br />
			<input type="text" name="description" value="<c:if test="${not empty entity}">${entity.description}</c:if>" /> Enter description *:<br />
			
			${locationFindSuccessMessage} 	
			${locationFindErrorMessage} <br />
			
			${locationUpdateSuccessMessage} 	
			${locationUpdateErrorMessage} <br />
			
			<input type="submit" value="Update location" />
	</form>
	
	
	<a href="Controller?command=location_show">Show location list</a>

	

</body>
</html>