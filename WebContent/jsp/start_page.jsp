<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>



<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="resources.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.start_page.head" var="head" />
<fmt:message bundle="${loc}" key="local.start_page.login_button"
	var="login_button" />
<fmt:message bundle="${loc}" key="local.start_page.registration_button"
	var="registration_button" />


<head>
<title>Travel Agency</title>
<link href="<c:url value="${path_style_css}" />" >
</head>

		<c:import url="${path_page_header}" charEncoding="utf-8" />

<body>
	<h2>${head}</h2>
	<hr />


<br/>
<a href="Controller?command=tour_show">Show tour list</a>
<br/>
<a href="Controller?command=order_show">Show order list</a>
<br/>


<a href="Controller?command=user_show">Show user list</a>
<br/>




	<hr />
	

	<c:import url="${path_page_footer}" charEncoding="utf-8" />


</body>
</html>