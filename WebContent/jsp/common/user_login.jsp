<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="resources.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.login_page.head" var="head" />
<fmt:message bundle="${loc}" key="local.login_page.label.login"
	var="label_login" />
<fmt:message bundle="${loc}" key="local.login_page.label.password"
	var="label_password" />
	
<fmt:message bundle="${loc}" key="local.user.login.page.button.login"
	var="button_login" />
	
	
<head>
<title>Login</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>

<body>
	<c:import url="${path_page_header}" charEncoding="utf-8" />
	<h1>${head}</h1>


	<form name="loginForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="login" /> ${label_login}<br />
		<input type="text" name="login" value="" /> <br />${label_password}<br />
		<input type="password" name="password" value="" /> <br />
		${errorLoginPassMessage}
		
		<input type="submit" value= "${button_login}" />
	</form>
	<hr />

	<c:import url="${path_page_footer}" charEncoding="utf-8" />
</body>
</html>
