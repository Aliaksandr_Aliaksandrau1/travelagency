<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<title>Locale</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="resources.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.locbutton.name.ru"
	var="ru_button" />
<fmt:message bundle="${loc}" key="local.locbutton.name.en"
	var="en_button" />
<c:set var="context_path" value="${pageContext.request.contextPath}"
	scope="session"></c:set>
<c:set var="path_uri" value="${pageContext.request.requestURI}"
	scope="session" />


</head>
<body>
	<table>
		<tr>
			<th>
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="i18n" /> <input
						type="hidden" name="local" value="ru" /> <input type="submit"
						value="${ru_button}" /><br />
				</form>
			</th>
			<th>
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="i18n" /> <input
						type="hidden" name="local" value="en" /> <input type="submit"
						value="${en_button}" /><br />
				</form>
			</th>
		</tr>
	</table>


</body>
</html>
