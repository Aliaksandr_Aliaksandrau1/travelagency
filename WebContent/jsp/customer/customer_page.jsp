<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.locale" var="loc" />

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">


<title>Customer page</title>
</head>
<body>
	
	<c:import url="${path_page_header}" charEncoding="utf-8" />
	

	<h1>Customer Page</h1>


	<jsp:useBean id="date" class="java.util.Date" />
	<p>
		The date/time is
		<%=date%>



	<br/>
		  
	  
		<c:out value="${sessionScope.user.role}"/> 
		
	


</body>
</html>