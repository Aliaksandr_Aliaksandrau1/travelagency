<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="resources.locale" var="loc" />
<fmt:message bundle="${loc}" key="local.registration_page.head" var="head" />
<fmt:message bundle="${loc}" key="local.user.registration.page.textfield.login" var="textfield_login" />




<html>
<head>
<title>Registration</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
<c:import url="${path_page_header}" charEncoding="utf-8" />
<h1>${head}</h1>
	
	
	
	<form name="RegistrationForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="user_registration" /> 
		<input type="text" name="login" value="" /> ${textfield_login}<br /> 
		<input type="password" name="password" value="" /> Enter Password *: <br />
		<input type="text" name="name" value="" /> Enter your name *: <br />
		<input type="text" name="surname" value="" /> Enter your surname *: <br />
		<input type="text" name="telnumber" value="" /> Enter telephone number *:<br /> 
		<input type="email" name="email" value="" /> Enter email number *:<br /> 
		
		
		<input type="hidden" name="discount" value="0" />
		<input type="hidden" name="role" value="customer" />
		
		${errorRegistrationMessage}
		<input type="submit" value="Registration" />
	</form>
	<hr />
</body>
</html>
