<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	
	<h1> Update user</h1>
	<form name="UserUpdateForm" method="POST" action="Controller">
			<input type="hidden" name="command" value="user_update" autocomplete="on" /> 
			<input type="hidden" name="id" value="<c:if test="${not empty entity}">${entity.id}</c:if>" /> 
			<input 	type="text" name="login" value="<c:if test="${not empty entity}">${entity.login}</c:if>" autofocus/> Enter login*: <br />
			<input 	type="text" name="password" value="<c:if test="${not empty entity}">${entity.password}</c:if>" /> Enter password*: <br />
			<input 	type="text" name="role" value="<c:if test="${not empty entity}">${entity.role}</c:if>" /> Enter role*: <br />
			<input 	type="text" name="name" value="<c:if test="${not empty entity}">${entity.name}</c:if>" /> Enter name*: <br />
			<input 	type="text" name="surname" value="<c:if test="${not empty entity}">${entity.surname}</c:if>" /> Enter surname*: <br />
			<input 	type="text" name="telnumber" value="<c:if test="${not empty entity}">${entity.telephoneNumber}</c:if>" /> Enter telephone number*: <br />
			<input 	type="text" name="email" value="<c:if test="${not empty entity}">${entity.email}</c:if>" /> Enter email*: <br />
			<input 	type="text" name="discount" value="<c:if test="${not empty entity}">${entity.discount}</c:if>" /> Enter discount*: <br />
			
			${locationFindSuccessMessage} 	
			${locationFindErrorMessage} <br />
			
			${locationUpdateSuccessMessage} 	
			${locationUpdateErrorMessage} <br />
			
			<input type="submit" value="Update user" />
	</form>
	
	
	<a href="Controller?command=user_show">Show user list</a>

	

</body>
</html>