<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
<link href="<c:url value="${path_style_css}" />" rel="stylesheet">

</head>
<body>

	<h1>User list</h1>

	<table>
		<tr>
			<th>ID</th>
			<th>Login</th>
			<th>Role</th>
			<th>Name</th>
			<th>Surname</th>
			<th>Tel number</th>
			<th>email</th>
			<th>Discount, %</th>

		</tr>
		<c:forEach var="variable" items="${entityList}">
			<tr>
				<td>${variable.id}</td>
				<td>${variable.login}</td>
  				<td>${variable.role}</td>
				<td>${variable.name}</td>
				<td>${variable.surname}</td>
				<td>${variable.telephoneNumber}</td>
				<td>${variable.email}</td>
				<td>${variable.discount}</td>

		
				<td>
					<table>
						<tr>
							<th>
								<form name="userDeleteForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="user_delete" />
									<input type="hidden" name="id" value="${variable.id}"> <input
										type="submit" value="Delete" />
								</form>
							</th>
							<th>

								<form name="userUpdateForm" method="POST"
									action="Controller">
									<input type="hidden" name="command" value="user_update_go" />
									<input type="hidden" name="id" value="${variable.id}"> <input
										type="submit" value="Update" />
								</form>

							</th>
						</tr>
					</table>

				</td>

			</tr>
		</c:forEach>


	</table>
	<br /> ${locationDeleteErrorMessage} ${locationDeleteSuccessMessage}
	${locationShowErrorMessage}
	<br />
	<table>
		<tr>
			<th><a href="Controller?command=user_create_go">Create
					new user</a></th>
			<th><a href="Controller?command=user_show">Refresh List</a></th>
		</tr>
	</table>

</body>
</html>