<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
 <link href="<c:url value="${path_style_css}" />" rel="stylesheet">
</head>
<body>
	
	<h1>Admin: Update tour status</h1>
	<form name="TourStatusUpdateForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="tourstatus_update" />
			<input type="hidden" name="id" value="<c:if test="${not empty entity}">${entity.id}</c:if>" /> 
			<input type="text" name="name" value="<c:if test="${not empty entity}">${entity.name}</c:if>" /> Enter name*: <br />
			
			${tourStatusUpdateSuccessMessage} 	
			${tourStatusUpdateErrorMessage} <br />
			
			<input type="submit" value="Update tour status" />
	</form>

<a href="Controller?command=tourstatus_show">Show tour status list</a>

</body>
</html>